/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50713
Source Host           : localhost:3306
Source Database       : market

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2020-04-09 15:02:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `aid` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员主键id',
  `aname` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------

-- ----------------------------
-- Table structure for `goods`
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `gid` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品主键id',
  `gname` varchar(255) DEFAULT NULL,
  `gprice` float DEFAULT NULL,
  `gnum` int(11) DEFAULT NULL,
  `gdetail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES ('1', '玉米', '2', '1250', '无损坏');
INSERT INTO `goods` VALUES ('2', '真果粒', '58', '300', '最近生产日期');
INSERT INTO `goods` VALUES ('3', '可口可乐', '3', '100', '未过期');
INSERT INTO `goods` VALUES ('4', '纯牛奶', '68', '100', '新鲜牛奶');
INSERT INTO `goods` VALUES ('5', '百威啤酒', '20', '500', '未过期');
INSERT INTO `goods` VALUES ('6', '酸奶', '20', '100', '最新');

-- ----------------------------
-- Table structure for `jinhuo`
-- ----------------------------
DROP TABLE IF EXISTS `jinhuo`;
CREATE TABLE `jinhuo` (
  `jid` int(11) NOT NULL AUTO_INCREMENT COMMENT '进货主键id',
  `gname` varchar(255) DEFAULT NULL COMMENT '商品名',
  `pid` int(11) DEFAULT NULL COMMENT '供应商id',
  `jnum` int(11) DEFAULT NULL,
  `jdate` date DEFAULT NULL,
  `jprice` float DEFAULT NULL COMMENT '进货单价',
  `jtotal` float DEFAULT NULL COMMENT '应付金额',
  `jmsg` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`jid`),
  KEY `fk_pid` (`pid`),
  CONSTRAINT `fk_pid` FOREIGN KEY (`pid`) REFERENCES `provide` (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jinhuo
-- ----------------------------
INSERT INTO `jinhuo` VALUES ('1', '玉米', '1', '300', '2020-04-07', '3', '900', '品质不错');
INSERT INTO `jinhuo` VALUES ('3', '可口可乐', '4', '100', '2020-04-07', '100', '300', '品质不错');
INSERT INTO `jinhuo` VALUES ('6', '纯牛奶', '2', '100', '2020-04-08', '68', '6800', '品质不错');
INSERT INTO `jinhuo` VALUES ('8', '酸奶', '2', '100', '2020-04-08', '6', '600', '最新');
INSERT INTO `jinhuo` VALUES ('9', '百威啤酒', '3', '100', '2020-04-08', '18', '1800', '品牌不错');
INSERT INTO `jinhuo` VALUES ('10', '百威啤酒', '3', '100', '2020-04-08', '18', '1800', '品牌不错');
INSERT INTO `jinhuo` VALUES ('11', '百威啤酒', '3', '100', '2020-04-08', '18', '1800', '品牌不错');
INSERT INTO `jinhuo` VALUES ('12', '百威啤酒', '3', '100', '2020-04-08', '18', '1800', '品牌不错');

-- ----------------------------
-- Table structure for `provide`
-- ----------------------------
DROP TABLE IF EXISTS `provide`;
CREATE TABLE `provide` (
  `pid` int(11) NOT NULL AUTO_INCREMENT COMMENT '供应商id',
  `pname` varchar(255) DEFAULT NULL,
  `ptelephone` varchar(255) DEFAULT NULL,
  `paddress` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of provide
-- ----------------------------
INSERT INTO `provide` VALUES ('1', '东北玉米厂', '66666', '山东省菏泽市');
INSERT INTO `provide` VALUES ('2', '蒙牛牛奶', '11111', '内蒙古');
INSERT INTO `provide` VALUES ('3', '百威啤酒公司', '22222', '美国');
INSERT INTO `provide` VALUES ('4', '可口可乐公司', '888888', '美国');

-- ----------------------------
-- Table structure for `sell`
-- ----------------------------
DROP TABLE IF EXISTS `sell`;
CREATE TABLE `sell` (
  `sid` int(11) NOT NULL AUTO_INCREMENT COMMENT '销售id',
  `gid` int(11) DEFAULT NULL COMMENT '商品id',
  `snum` int(11) DEFAULT NULL,
  `sdate` date DEFAULT NULL,
  `sprice` float DEFAULT NULL COMMENT '销售单价',
  `stotal` float DEFAULT NULL COMMENT '应付金额',
  `smsg` varchar(255) DEFAULT NULL COMMENT '销售信息备注',
  PRIMARY KEY (`sid`),
  KEY `fk_sgid` (`gid`),
  CONSTRAINT `fk_sgid` FOREIGN KEY (`gid`) REFERENCES `goods` (`gid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sell
-- ----------------------------

-- ----------------------------
-- Table structure for `tuihuo`
-- ----------------------------
DROP TABLE IF EXISTS `tuihuo`;
CREATE TABLE `tuihuo` (
  `tid` int(11) NOT NULL AUTO_INCREMENT COMMENT '退货表id',
  `sid` int(11) DEFAULT NULL COMMENT '销售信息id',
  `tnum` int(11) DEFAULT NULL COMMENT '退货数量',
  `tdate` date DEFAULT NULL COMMENT '退货时间',
  `treason` varchar(255) DEFAULT NULL COMMENT '退货原因',
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tuihuo
-- ----------------------------
INSERT INTO `tuihuo` VALUES ('1', '1', '1', '2020-04-07', '不想要了');
INSERT INTO `tuihuo` VALUES ('2', null, null, '2020-04-07', '');
