package com.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Marketserver7001Application {

    public static void main(String[] args) {
        SpringApplication.run(Marketserver7001Application.class, args);
    }

}
