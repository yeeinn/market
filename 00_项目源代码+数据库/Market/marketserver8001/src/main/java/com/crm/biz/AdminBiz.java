package com.crm.biz;

import com.crm.entity.Admin;

public interface AdminBiz {
    public Admin login(String aname,String pwd);
    public Admin getAdmin(Integer aid);
    public int updatePwd(Admin admin);
}
