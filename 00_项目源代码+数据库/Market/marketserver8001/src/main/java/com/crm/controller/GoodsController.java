package com.crm.controller;

import com.crm.biz.GoodsBiz;
import com.crm.entity.Goods;
import com.crm.entity.Jinhuo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: jm
 * \* Date: 2020/4/6
 * \* Time: 21:50
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
public class GoodsController {
    @Resource
    private GoodsBiz goodsBiz;

    @GetMapping("allgoods")
    public Map<String,Object> findAll(){
        Map<String,Object> map=new HashMap<>();
        List<Goods> goods=goodsBiz.findAll();
        map.put("goods",goods);
        return map;
    }

    @PostMapping("addGoods")
    public Map<String,Object> addGoods(@RequestBody Goods goods){
        Map<String,Object> map=new HashMap<>();
        List<Goods> goodsList=goodsBiz.findAll();
        String gname=goods.getGname();
        for (int i=0;i<goodsList.size();i++) {
            if (gname.equals(goodsList.get(i).getGname())) {
                map.put("msg","error");
                //System.out.println("该商品已存在------------------"+map);
                return map;
            }
        }
        goodsBiz.addGoods(goods);
        map.put("goods",goods);
        //System.out.println("8001----------------"+goods);
        return map;
    }

    @GetMapping("findById")
    public Map<String,Object> findById(@RequestParam Integer gid){
        //System.out.println("6001---------"+gid);
        Map<String,Object> map=new HashMap<>();
        List<Goods> good=goodsBiz.findById(gid);
        //System.out.println("8001-----------"+good.get(0).getGname());
        map.put("good",good);
        return map;
    }

    @PostMapping("modGoods")
    public Map<String,Object> modGoods(@RequestBody Goods goods){
        Integer gid=goods.getGid();
        Goods good=goodsBiz.getGood(gid);
        good.setGnum(goods.getGnum());
        good.setGprice(goods.getGprice());
        good.setGdetail(goods.getGdetail());
        goodsBiz.modGoods(good);
        Map<String,Object> map=new HashMap<>();
        map.put("good",good);
        return map;
    }

    @PostMapping("deleteGoods")
    public Map<String,Object> deleteGoods(@RequestParam Integer gid){
        Map<String,Object> map=new HashMap<>();
        goodsBiz.deleteGoods(gid);
        map.put("msg","success");
        return map;
    }

    @RequestMapping("deleteByJinhuo")
    public boolean delete(String gname, int gnum){
        return goodsBiz.updateGoods2(gname, gnum)>0;
    }

    @RequestMapping("updateByJinhuo")
    public boolean update(String gname, int gnum){
        return goodsBiz.updateGoods(gname, gnum)>0;
    }

    @RequestMapping("queryByJinhuo")
    public int query(String gname){
        return goodsBiz.queryGnameCount(gname);
    }

    @RequestMapping("insertByJinhuo")
    public int insert(String gname, int gnum){
        return goodsBiz.insertGoods(gname, gnum);
    }

    @PostMapping("queryOneGood")
    public Goods queryOneGood(@RequestParam int gid){
        return  goodsBiz.getGood(gid);
    }


    @RequestMapping("updateBySell")
    @ResponseBody
    public int updateBySelladd(@RequestBody Goods goods){
        return goodsBiz.updateGoodsBySelladd(goods);
    }

}
