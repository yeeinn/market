package com.crm.biz.impl;

import com.crm.biz.AdminBiz;
import com.crm.dao.AdminMapper;
import com.crm.entity.Admin;
import com.crm.entity.AdminExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: jm
 * \* Date: 2020/4/5
 * \* Time: 22:13
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Service
public class AdminBizImpl implements AdminBiz {
    @Resource
    private AdminMapper adminMapper;

    @Override
    public Admin login(String aname, String pwd) {
        AdminExample adminExample=new AdminExample();
        adminExample.createCriteria().andAnameEqualTo(aname).andPwdEqualTo(pwd);
        List<Admin> adminList=adminMapper.selectByExample(adminExample);
        if(adminList.size()>0){
            return adminList.get(0);
        }
        return null;
    }

    @Override
    public Admin getAdmin(Integer aid) {
        return adminMapper.selectByPrimaryKey(aid);
    }

    @Override
    public int updatePwd(Admin admin) {
        return adminMapper.updateByPrimaryKey(admin);
    }
}
