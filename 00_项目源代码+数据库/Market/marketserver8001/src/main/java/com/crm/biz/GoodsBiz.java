package com.crm.biz;

import com.crm.entity.Goods;
import com.crm.entity.GoodsExample;

import java.util.Date;
import java.util.List;

public interface GoodsBiz {
    public List<Goods> findAll();
    public int addGoods(Goods goods);
    public List<Goods> findById(Integer gid);
    public Goods getGood(Integer gid);
    public int modGoods(Goods goods);
    public int deleteGoods(Integer gid);

    public int queryGnameCount(String gname);
    public int updateGoods(String gname,int gnum);
    public int insertGoods(String gname, int gnum);
    public int updateGoods2(String gname,int gnum);

    public  int updateGoodsBySelladd(Goods goods);

}
