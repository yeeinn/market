package com.crm.biz.impl;

import com.crm.biz.GoodsBiz;
import com.crm.dao.GoodsMapper;
import com.crm.entity.Goods;
import com.crm.entity.GoodsExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: jm
 * \* Date: 2020/4/6
 * \* Time: 21:47
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Service
public class GoodsBizImpl implements GoodsBiz {
    @Resource
    private GoodsMapper goodsMapper;

    @Override
    public List<Goods> findAll() {
        return goodsMapper.selectByExample(null);
    }

    @Override
    public int addGoods(Goods goods) {
        return goodsMapper.insert(goods);
    }

    @Override
    public List<Goods> findById(Integer gid) {
        GoodsExample goodsExample = new GoodsExample();
        goodsExample.createCriteria().andGidEqualTo(gid);
        return goodsMapper.selectByExample(goodsExample);
    }

    @Override
    public Goods getGood(Integer gid) {
        GoodsExample example = new GoodsExample();
        example.createCriteria().andGidEqualTo(gid);
        return goodsMapper.selectByPrimaryKey(gid);
    }

    @Override
    public int modGoods(Goods goods) {
        return goodsMapper.updateByPrimaryKey(goods);
    }

    @Override
    public int deleteGoods(Integer gid) {
        return goodsMapper.deleteByPrimaryKey(gid);
    }

    @Override
    public int queryGnameCount(String gname) {
        return goodsMapper.queryGnameCount(gname);
    }

    @Override
    public int updateGoods(String gname, int gnum) {
        return goodsMapper.updateGoods(gname, gnum);
    }

    @Override
    public int insertGoods(String gname, int gnum) {
        return goodsMapper.insertGoods(gname, gnum);
    }

    @Override
    public int updateGoods2(String gname, int gnum) {
        return goodsMapper.updateGoods2(gname, gnum);
    }

    public int updateGoodsBySelladd(Goods goods){
        return goodsMapper.updateGoodsBySell(goods);
    }

}
