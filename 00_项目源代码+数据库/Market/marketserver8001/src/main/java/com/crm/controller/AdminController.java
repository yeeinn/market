package com.crm.controller;

import com.crm.biz.AdminBiz;
import com.crm.entity.Admin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: jm
 * \* Date: 2020/4/5
 * \* Time: 22:10
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
public class AdminController {
    @Resource
    private AdminBiz adminBiz;

    @PostMapping("login")
    public Map<String,Object> login(@RequestParam String aname, @RequestParam String pwd){
        Map<String,Object> map=new HashMap<>();
        Admin admin=adminBiz.login(aname,pwd);
        if(admin!=null){
            map.put("admin",admin);
            map.put("msg","success");
        }else{
            map.put("msg","error");
        }
        return map;
    }

    @PostMapping("updatePwd")
    public Map<String,Object> updatePwd(@RequestBody Admin admin){
        Map<String,Object> map=new HashMap<>();
        Integer aid=admin.getAid();
        //System.out.println("6001----------"+admin.getPwd());
        String pwd=admin.getPwd();
        Admin admin1=adminBiz.getAdmin(aid);
        //System.out.println("8001---------"+admin1.getPwd());
        admin1.setPwd(pwd);
        adminBiz.updatePwd(admin1);
        map.put("admin1",admin1);
        return map;
    }
}
