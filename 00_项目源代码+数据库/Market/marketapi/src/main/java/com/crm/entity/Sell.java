package com.crm.entity;

import java.util.Date;

public class Sell {
    private Integer sid;

    private Goods good;

    private Integer snum;

    private String sdate;

    private Float sprice;

    private Float stotal;

    private String smsg;

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public void setGood(Goods good) {
        this.good = good;
    }

    public Goods getGood() {
        return good;
    }

    public Integer getSnum() {
        return snum;
    }

    public void setSnum(Integer snum) {
        this.snum = snum;
    }

    public String getSdate() {
        return sdate;
    }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public Float getSprice() {
        return sprice;
    }

    public void setSprice(Float sprice) {
        this.sprice = sprice;
    }

    public Float getStotal() {
        return stotal;
    }

    public void setStotal(Float stotal) {
        this.stotal = stotal;
    }

    public String getSmsg() {
        return smsg;
    }

    public void setSmsg(String smsg) {
        this.smsg = smsg == null ? null : smsg.trim();
    }
}