package com.crm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Jinhuo {
    private Integer jid;

    private String gname;

    private Integer pid;

    private Integer jnum;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date jdate;

    private Float jprice;

    private Float jtotal;

    private String jmsg;

    private Goods goods;

    private Provide provide;

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public Provide getProvide() {
        return provide;
    }

    public void setProvide(Provide provide) {
        this.provide = provide;
    }

    public Integer getJid() {
        return jid;
    }

    public void setJid(Integer jid) {
        this.jid = jid;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getJnum() {
        return jnum;
    }

    public void setJnum(Integer jnum) {
        this.jnum = jnum;
    }

    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    public Date getJdate() {
        return jdate;
    }

    public void setJdate(Date jdate) {
        this.jdate = jdate;
    }

    public Float getJprice() {
        return jprice;
    }

    public void setJprice(Float jprice) {
        this.jprice = jprice;
    }

    public Float getJtotal() {
        return jtotal;
    }

    public void setJtotal(Float jtotal) {
        this.jtotal = jtotal;
    }

    public String getJmsg() {
        return jmsg;
    }

    public void setJmsg(String jmsg) {
        this.jmsg = jmsg == null ? null : jmsg.trim();
    }
}