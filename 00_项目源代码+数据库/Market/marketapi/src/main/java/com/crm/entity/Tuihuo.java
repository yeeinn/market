package com.crm.entity;

public class Tuihuo {
    private Integer tid;

    private Integer sid;

    private Integer tnum;

    private String tdate;

    private String treason;

    private Sell sell;

    private Goods good;

    public Goods getGood() {
        return good;
    }

    public void setGood(Goods good) {
        this.good = good;
    }

    public Sell getSell() {
        return sell;
    }

    public void setSell(Sell sell) {
        this.sell = sell;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public Integer getTnum() {
        return tnum;
    }

    public void setTnum(Integer tnum) {
        this.tnum = tnum;
    }

    public String getTdate() {
        return tdate;
    }

    public void setTdate(String tdate) {
        this.tdate = tdate == null ? null : tdate.trim();
    }

    public String getTreason() {
        return treason;
    }

    public void setTreason(String treason) {
        this.treason = treason == null ? null : treason.trim();
    }
}