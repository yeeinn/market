package com.crm.entity;

public class Provide {
    private Integer pid;

    private String pname;

    private String ptelephone;

    private String paddress;

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname == null ? null : pname.trim();
    }

    public String getPtelephone() {
        return ptelephone;
    }

    public void setPtelephone(String ptelephone) {
        this.ptelephone = ptelephone == null ? null : ptelephone.trim();
    }

    public String getPaddress() {
        return paddress;
    }

    public void setPaddress(String paddress) {
        this.paddress = paddress == null ? null : paddress.trim();
    }
}