package com.crm.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class JinhuoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public JinhuoExample() {
        oredCriteria = new ArrayList<>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andJidIsNull() {
            addCriterion("jid is null");
            return (Criteria) this;
        }

        public Criteria andJidIsNotNull() {
            addCriterion("jid is not null");
            return (Criteria) this;
        }

        public Criteria andJidEqualTo(Integer value) {
            addCriterion("jid =", value, "jid");
            return (Criteria) this;
        }

        public Criteria andJidNotEqualTo(Integer value) {
            addCriterion("jid <>", value, "jid");
            return (Criteria) this;
        }

        public Criteria andJidGreaterThan(Integer value) {
            addCriterion("jid >", value, "jid");
            return (Criteria) this;
        }

        public Criteria andJidGreaterThanOrEqualTo(Integer value) {
            addCriterion("jid >=", value, "jid");
            return (Criteria) this;
        }

        public Criteria andJidLessThan(Integer value) {
            addCriterion("jid <", value, "jid");
            return (Criteria) this;
        }

        public Criteria andJidLessThanOrEqualTo(Integer value) {
            addCriterion("jid <=", value, "jid");
            return (Criteria) this;
        }

        public Criteria andJidIn(List<Integer> values) {
            addCriterion("jid in", values, "jid");
            return (Criteria) this;
        }

        public Criteria andJidNotIn(List<Integer> values) {
            addCriterion("jid not in", values, "jid");
            return (Criteria) this;
        }

        public Criteria andJidBetween(Integer value1, Integer value2) {
            addCriterion("jid between", value1, value2, "jid");
            return (Criteria) this;
        }

        public Criteria andJidNotBetween(Integer value1, Integer value2) {
            addCriterion("jid not between", value1, value2, "jid");
            return (Criteria) this;
        }

        public Criteria andGnameIsNull() {
            addCriterion("gname is null");
            return (Criteria) this;
        }

        public Criteria andGnameIsNotNull() {
            addCriterion("gname is not null");
            return (Criteria) this;
        }

        public Criteria andGnameEqualTo(String value) {
            addCriterion("gname =", value, "gname");
            return (Criteria) this;
        }

        public Criteria andGnameNotEqualTo(String value) {
            addCriterion("gname <>", value, "gname");
            return (Criteria) this;
        }

        public Criteria andGnameGreaterThan(String value) {
            addCriterion("gname >", value, "gname");
            return (Criteria) this;
        }

        public Criteria andGnameGreaterThanOrEqualTo(String value) {
            addCriterion("gname >=", value, "gname");
            return (Criteria) this;
        }

        public Criteria andGnameLessThan(String value) {
            addCriterion("gname <", value, "gname");
            return (Criteria) this;
        }

        public Criteria andGnameLessThanOrEqualTo(String value) {
            addCriterion("gname <=", value, "gname");
            return (Criteria) this;
        }

        public Criteria andGnameLike(String value) {
            addCriterion("gname like", value, "gname");
            return (Criteria) this;
        }

        public Criteria andGnameNotLike(String value) {
            addCriterion("gname not like", value, "gname");
            return (Criteria) this;
        }

        public Criteria andGnameIn(List<String> values) {
            addCriterion("gname in", values, "gname");
            return (Criteria) this;
        }

        public Criteria andGnameNotIn(List<String> values) {
            addCriterion("gname not in", values, "gname");
            return (Criteria) this;
        }

        public Criteria andGnameBetween(String value1, String value2) {
            addCriterion("gname between", value1, value2, "gname");
            return (Criteria) this;
        }

        public Criteria andGnameNotBetween(String value1, String value2) {
            addCriterion("gname not between", value1, value2, "gname");
            return (Criteria) this;
        }

        public Criteria andPidIsNull() {
            addCriterion("pid is null");
            return (Criteria) this;
        }

        public Criteria andPidIsNotNull() {
            addCriterion("pid is not null");
            return (Criteria) this;
        }

        public Criteria andPidEqualTo(Integer value) {
            addCriterion("pid =", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidNotEqualTo(Integer value) {
            addCriterion("pid <>", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidGreaterThan(Integer value) {
            addCriterion("pid >", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidGreaterThanOrEqualTo(Integer value) {
            addCriterion("pid >=", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidLessThan(Integer value) {
            addCriterion("pid <", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidLessThanOrEqualTo(Integer value) {
            addCriterion("pid <=", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidIn(List<Integer> values) {
            addCriterion("pid in", values, "pid");
            return (Criteria) this;
        }

        public Criteria andPidNotIn(List<Integer> values) {
            addCriterion("pid not in", values, "pid");
            return (Criteria) this;
        }

        public Criteria andPidBetween(Integer value1, Integer value2) {
            addCriterion("pid between", value1, value2, "pid");
            return (Criteria) this;
        }

        public Criteria andPidNotBetween(Integer value1, Integer value2) {
            addCriterion("pid not between", value1, value2, "pid");
            return (Criteria) this;
        }

        public Criteria andJnumIsNull() {
            addCriterion("jnum is null");
            return (Criteria) this;
        }

        public Criteria andJnumIsNotNull() {
            addCriterion("jnum is not null");
            return (Criteria) this;
        }

        public Criteria andJnumEqualTo(Integer value) {
            addCriterion("jnum =", value, "jnum");
            return (Criteria) this;
        }

        public Criteria andJnumNotEqualTo(Integer value) {
            addCriterion("jnum <>", value, "jnum");
            return (Criteria) this;
        }

        public Criteria andJnumGreaterThan(Integer value) {
            addCriterion("jnum >", value, "jnum");
            return (Criteria) this;
        }

        public Criteria andJnumGreaterThanOrEqualTo(Integer value) {
            addCriterion("jnum >=", value, "jnum");
            return (Criteria) this;
        }

        public Criteria andJnumLessThan(Integer value) {
            addCriterion("jnum <", value, "jnum");
            return (Criteria) this;
        }

        public Criteria andJnumLessThanOrEqualTo(Integer value) {
            addCriterion("jnum <=", value, "jnum");
            return (Criteria) this;
        }

        public Criteria andJnumIn(List<Integer> values) {
            addCriterion("jnum in", values, "jnum");
            return (Criteria) this;
        }

        public Criteria andJnumNotIn(List<Integer> values) {
            addCriterion("jnum not in", values, "jnum");
            return (Criteria) this;
        }

        public Criteria andJnumBetween(Integer value1, Integer value2) {
            addCriterion("jnum between", value1, value2, "jnum");
            return (Criteria) this;
        }

        public Criteria andJnumNotBetween(Integer value1, Integer value2) {
            addCriterion("jnum not between", value1, value2, "jnum");
            return (Criteria) this;
        }

        public Criteria andJdateIsNull() {
            addCriterion("jdate is null");
            return (Criteria) this;
        }

        public Criteria andJdateIsNotNull() {
            addCriterion("jdate is not null");
            return (Criteria) this;
        }

        public Criteria andJdateEqualTo(Date value) {
            addCriterionForJDBCDate("jdate =", value, "jdate");
            return (Criteria) this;
        }

        public Criteria andJdateNotEqualTo(Date value) {
            addCriterionForJDBCDate("jdate <>", value, "jdate");
            return (Criteria) this;
        }

        public Criteria andJdateGreaterThan(Date value) {
            addCriterionForJDBCDate("jdate >", value, "jdate");
            return (Criteria) this;
        }

        public Criteria andJdateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("jdate >=", value, "jdate");
            return (Criteria) this;
        }

        public Criteria andJdateLessThan(Date value) {
            addCriterionForJDBCDate("jdate <", value, "jdate");
            return (Criteria) this;
        }

        public Criteria andJdateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("jdate <=", value, "jdate");
            return (Criteria) this;
        }

        public Criteria andJdateIn(List<Date> values) {
            addCriterionForJDBCDate("jdate in", values, "jdate");
            return (Criteria) this;
        }

        public Criteria andJdateNotIn(List<Date> values) {
            addCriterionForJDBCDate("jdate not in", values, "jdate");
            return (Criteria) this;
        }

        public Criteria andJdateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("jdate between", value1, value2, "jdate");
            return (Criteria) this;
        }

        public Criteria andJdateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("jdate not between", value1, value2, "jdate");
            return (Criteria) this;
        }

        public Criteria andJpriceIsNull() {
            addCriterion("jprice is null");
            return (Criteria) this;
        }

        public Criteria andJpriceIsNotNull() {
            addCriterion("jprice is not null");
            return (Criteria) this;
        }

        public Criteria andJpriceEqualTo(Float value) {
            addCriterion("jprice =", value, "jprice");
            return (Criteria) this;
        }

        public Criteria andJpriceNotEqualTo(Float value) {
            addCriterion("jprice <>", value, "jprice");
            return (Criteria) this;
        }

        public Criteria andJpriceGreaterThan(Float value) {
            addCriterion("jprice >", value, "jprice");
            return (Criteria) this;
        }

        public Criteria andJpriceGreaterThanOrEqualTo(Float value) {
            addCriterion("jprice >=", value, "jprice");
            return (Criteria) this;
        }

        public Criteria andJpriceLessThan(Float value) {
            addCriterion("jprice <", value, "jprice");
            return (Criteria) this;
        }

        public Criteria andJpriceLessThanOrEqualTo(Float value) {
            addCriterion("jprice <=", value, "jprice");
            return (Criteria) this;
        }

        public Criteria andJpriceIn(List<Float> values) {
            addCriterion("jprice in", values, "jprice");
            return (Criteria) this;
        }

        public Criteria andJpriceNotIn(List<Float> values) {
            addCriterion("jprice not in", values, "jprice");
            return (Criteria) this;
        }

        public Criteria andJpriceBetween(Float value1, Float value2) {
            addCriterion("jprice between", value1, value2, "jprice");
            return (Criteria) this;
        }

        public Criteria andJpriceNotBetween(Float value1, Float value2) {
            addCriterion("jprice not between", value1, value2, "jprice");
            return (Criteria) this;
        }

        public Criteria andJtotalIsNull() {
            addCriterion("jtotal is null");
            return (Criteria) this;
        }

        public Criteria andJtotalIsNotNull() {
            addCriterion("jtotal is not null");
            return (Criteria) this;
        }

        public Criteria andJtotalEqualTo(Float value) {
            addCriterion("jtotal =", value, "jtotal");
            return (Criteria) this;
        }

        public Criteria andJtotalNotEqualTo(Float value) {
            addCriterion("jtotal <>", value, "jtotal");
            return (Criteria) this;
        }

        public Criteria andJtotalGreaterThan(Float value) {
            addCriterion("jtotal >", value, "jtotal");
            return (Criteria) this;
        }

        public Criteria andJtotalGreaterThanOrEqualTo(Float value) {
            addCriterion("jtotal >=", value, "jtotal");
            return (Criteria) this;
        }

        public Criteria andJtotalLessThan(Float value) {
            addCriterion("jtotal <", value, "jtotal");
            return (Criteria) this;
        }

        public Criteria andJtotalLessThanOrEqualTo(Float value) {
            addCriterion("jtotal <=", value, "jtotal");
            return (Criteria) this;
        }

        public Criteria andJtotalIn(List<Float> values) {
            addCriterion("jtotal in", values, "jtotal");
            return (Criteria) this;
        }

        public Criteria andJtotalNotIn(List<Float> values) {
            addCriterion("jtotal not in", values, "jtotal");
            return (Criteria) this;
        }

        public Criteria andJtotalBetween(Float value1, Float value2) {
            addCriterion("jtotal between", value1, value2, "jtotal");
            return (Criteria) this;
        }

        public Criteria andJtotalNotBetween(Float value1, Float value2) {
            addCriterion("jtotal not between", value1, value2, "jtotal");
            return (Criteria) this;
        }

        public Criteria andJmsgIsNull() {
            addCriterion("jmsg is null");
            return (Criteria) this;
        }

        public Criteria andJmsgIsNotNull() {
            addCriterion("jmsg is not null");
            return (Criteria) this;
        }

        public Criteria andJmsgEqualTo(String value) {
            addCriterion("jmsg =", value, "jmsg");
            return (Criteria) this;
        }

        public Criteria andJmsgNotEqualTo(String value) {
            addCriterion("jmsg <>", value, "jmsg");
            return (Criteria) this;
        }

        public Criteria andJmsgGreaterThan(String value) {
            addCriterion("jmsg >", value, "jmsg");
            return (Criteria) this;
        }

        public Criteria andJmsgGreaterThanOrEqualTo(String value) {
            addCriterion("jmsg >=", value, "jmsg");
            return (Criteria) this;
        }

        public Criteria andJmsgLessThan(String value) {
            addCriterion("jmsg <", value, "jmsg");
            return (Criteria) this;
        }

        public Criteria andJmsgLessThanOrEqualTo(String value) {
            addCriterion("jmsg <=", value, "jmsg");
            return (Criteria) this;
        }

        public Criteria andJmsgLike(String value) {
            addCriterion("jmsg like", value, "jmsg");
            return (Criteria) this;
        }

        public Criteria andJmsgNotLike(String value) {
            addCriterion("jmsg not like", value, "jmsg");
            return (Criteria) this;
        }

        public Criteria andJmsgIn(List<String> values) {
            addCriterion("jmsg in", values, "jmsg");
            return (Criteria) this;
        }

        public Criteria andJmsgNotIn(List<String> values) {
            addCriterion("jmsg not in", values, "jmsg");
            return (Criteria) this;
        }

        public Criteria andJmsgBetween(String value1, String value2) {
            addCriterion("jmsg between", value1, value2, "jmsg");
            return (Criteria) this;
        }

        public Criteria andJmsgNotBetween(String value1, String value2) {
            addCriterion("jmsg not between", value1, value2, "jmsg");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {
        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}