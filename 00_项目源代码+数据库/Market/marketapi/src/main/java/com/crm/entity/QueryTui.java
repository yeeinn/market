package com.crm.entity;


public class QueryTui {
    private String content;
    private String date;
    private Integer number;
    private Integer rows=5;//每页显示的条数
    private  Integer start;//偏移量
    private  Integer page=1;
    private Integer last;//上一页
    private Integer next;//下一页；
    private Integer pageCount;//总页数

    @Override
    public String toString() {
        return "QueryTui{" +
                "content='" + content + '\'' +
                ", date='" + date + '\'' +
                ", number=" + number +
                ", rows=" + rows +
                ", start=" + start +
                ", page=" + page +
                ", last=" + last +
                ", next=" + next +
                ", pageCount=" + pageCount +
                '}';
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLast() {
        return last;
    }

    public void setLast(Integer last) {
        this.last = last;
    }

    public Integer getNext() {
        return next;
    }

    public void setNext(Integer next) {
        this.next = next;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }
}