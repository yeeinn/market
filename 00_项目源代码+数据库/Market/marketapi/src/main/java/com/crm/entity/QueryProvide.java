package com.crm.entity;

import java.io.Serializable;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: skj
 * \* Date: 2020/3/6
 * \* Time: 9:30
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class QueryProvide {
    private String name;
    private String address;
    private Integer rows = 5;
    private Integer start;
    private Integer page = 1;
    private Integer last;//
    private Integer next;//
    private Integer pagecount;//总页数

    public QueryProvide() {
    }

    public QueryProvide(String name, String address, Integer rows, Integer start, Integer page, Integer last, Integer next, Integer pagecount) {
        this.name =name;
        this.address = address;
        this.rows = rows;
        this.start = start;
        this.page = page;
        this.last = last;
        this.next = next;
        this.pagecount = pagecount;
    }


    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLast() {
        return last;
    }

    public void setLast(Integer last) {
        this.last = last;
    }

    public Integer getNext() {
        return next;
    }

    public void setNext(Integer next) {
        this.next = next;
    }

    public Integer getPagecount() {
        return pagecount;
    }

    public void setPagecount(Integer pagecount) {
        this.pagecount = pagecount;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}