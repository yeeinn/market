package com.crm.entity;

public class Goods {
    private Integer gid;

    private String gname;

    private Float gprice;

    private Integer gnum;

    private String gdetail;

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname == null ? null : gname.trim();
    }

    public Float getGprice() {
        return gprice;
    }

    public void setGprice(Float gprice) {
        this.gprice = gprice;
    }

    public Integer getGnum() {
        return gnum;
    }

    public void setGnum(Integer gnum) {
        this.gnum = gnum;
    }

    public String getGdetail() {
        return gdetail;
    }

    public void setGdetail(String gdetail) {
        this.gdetail = gdetail == null ? null : gdetail.trim();
    }
}