package com.crm.entity;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zjx
 * \* Date: 2020/4/7
 * \* Time: 15:36
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class QueryJinhuo {
    private String gname;
    private String jdate;
    private Integer rows=4;  //每页显示4条
    private Integer start;  //偏移量
    private Integer page=1;
    private Integer last;  //上一页
    private Integer next;   //下一页
    private Integer pageCount;  //总页数

    @Override
    public String toString() {
        return "QueryJinhuo{" +
                "gname='" + gname + '\'' +
                ", jdate='" + jdate + '\'' +
                ", rows=" + rows +
                ", start=" + start +
                ", page=" + page +
                ", last=" + last +
                ", next=" + next +
                ", pageCount=" + pageCount +
                '}';
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getJdate() {
        return jdate;
    }

    public void setJdate(String jdate) {
        this.jdate = jdate;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLast() {
        return last;
    }

    public void setLast(Integer last) {
        this.last = last;
    }

    public Integer getNext() {
        return next;
    }

    public void setNext(Integer next) {
        this.next = next;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }
}

