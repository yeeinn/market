<%--
  Created by IntelliJ IDEA.
  User: jm
  Date: 2020/4/5
  Time: 22:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>库存管理</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="assets/css/lib/jsgrid/jsgrid-theme.min.css" rel="stylesheet" />
    <link href="assets/css/lib/jsgrid/jsgrid.min.css" type="text/css" rel="stylesheet" />
    <link href="assets/css/lib/menubar/sidebar.css" rel="stylesheet">
    <link href="assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/lib/helper.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        var str="${msg}";
        if(str!=""){
            alert(str);
        }

        function mod(id) {
            $.post("findById",{"gid":id},function(data){
                if(data){
                    $("#gid").val(data[0].gid);
                    $("#gname").val(data[0].gname);
                    $("#gnum").val(data[0].gnum);
                    $("#gprice").val(data[0].gprice)
                    $("#gdetail").val(data[0].gdetail);
                }
            });
        }

        function deleteGoods(id) {
            $.post("findById",{"gid":id},function(data){
                if(data){
                    $("#gid1").val(data[0].gid);
                    document.getElementById("gname1").innerText=data[0].gname;
                    document.getElementById("gnum1").innerText=data[0].gnum;
                    document.getElementById("gprice1").innerText=data[0].gprice;
                    document.getElementById("gdetail1").innerText=data[0].gdetail;
                }
            });
        }
    </script>

</head>

<body>
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <div class="logo"><a href="toindex"><!-- <img src="assets/images/logo.png" alt="" /> --><span>CRM</span></a></div>
            <ul>
                <li class="label">Goods</li>
                <li><a class="sidebar-sub-toggle"><i class="ti-home"></i> 供应商管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryByProvide">查看供应商信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-panel"></i> 进货管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryByPage">查看进货信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-layout-grid4-alt"></i> 库存管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="allgoods">查看库存</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-money"></i> 销售管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="querySell/1">查看销售信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-view-list-alt"></i> 退货管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryTHs">查看退货信息</a></li>
                    </ul>
                </li>
                <li class="label">Person</li>
                <li><a class="sidebar-sub-toggle"><i class="ti-user"></i>个人中心<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="toupdatePwd">修改密码</a></li>
                    </ul>
                </li>
                <li class="label">Exit</li>
                <li><a href="tologin"><i class="ti-close"></i> 退出系统 </a></li>
            </ul>
        </div>
    </div>
</div>
<!-- /# sidebar -->

<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="float-left">
                    <div class="hamburger sidebar-toggle">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
                <div class="float-right">
                    <div>
                        <div class="header-icon">
                            <span class="user-avatar"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;${admin.aname}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-wrap">
    <div class="main">
        <div class="col-md-12">
        <div class="container-fluid">
            <div id="main-content">
                <table class="table-striped" style="width: 100%;">
                    <thead>
                    <tr style="height:48px;">
                        <th>商品名称</th>
                        <th>商品数量</th>
                        <th>商品单价</th>
                        <th>商品详情</th>
                        <th>
                            <a href="" data-toggle="modal" data-target="#addGoods">
                                <span class="glyphicon glyphicon-pencil" style="color: #ADADAD"></span>
                            </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${goods}" var="g">
                    <tr style="height: 32px;">
                        <td>${g.gname}</td>
                        <td>${g.gnum}</td>
                        <td>￥${g.gprice}</td>
                        <td>${g.gdetail}</td>
                        <td>
                            <a href="" data-toggle="modal" data-target="#modGoods" onclick="mod(${g.gid})">
                                <span class="glyphicon glyphicon-edit" style="color: #ADADAD"></span>
                            </a>&nbsp;&nbsp;&nbsp;
                            <a href="" data-toggle="modal" data-target="#deleteGoods" onclick="deleteGoods(${g.gid})">
                                <span class="glyphicon glyphicon-trash" style="color: #ADADAD"></span>
                            </a>&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>

    <!--添加商品信息模态框（Modal） -->
    <div class="modal fade" id="addGoods" tabindex="-1" role="dialog" aria-labelledby="addGoodsLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="addGoodsLabel">
                        添加商品
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                </div>
                <form action="addGoods" method="post">
                <div class="modal-body">
                    <input type="text" name="gname" class="form-control" placeholder="商品名称"/><br>
                    <input type="text" name="gnum" class="form-control" placeholder="商品数量"/><br>
                    <input type="text" name="gprice" class="form-control" placeholder="商品单价"/><br>
                    <input type="text" name="gdetail" class="form-control" placeholder="商品详情"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消
                    </button>
                    <button type="submit" class="btn btn-primary">
                        添加
                    </button>
                </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>

    <!--修改商品信息模态框（Modal） -->
    <div class="modal fade" id="modGoods" tabindex="-1" role="dialog" aria-labelledby="modGoodsLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modGoodsLabel">
                        修改商品信息
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                </div>
                <form action="modGoods" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="gid" class="form-control" id="gid"/>
                        <input type="text" name="gname" class="form-control" id="gname" readonly="readonly" /><br>
                        <input type="text" name="gnum" class="form-control" id="gnum" placeholder="商品数量" /><br>
                        <input type="text" name="gprice" class="form-control" id="gprice" placeholder="商品价格"/><br>
                        <input type="text" name="gdetail" class="form-control" id="gdetail" placeholder="商品详情"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消
                        </button>
                        <button type="submit" class="btn btn-primary">
                            修改
                        </button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>

    <!--删除商品信息模态框（Modal） -->
    <div class="modal fade" id="deleteGoods" tabindex="-1" role="dialog" aria-labelledby="deleteGoodsLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="deleteGoodsLabel">
                        删除商品信息
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                </div>
                <form action="deleteGoods" method="post">
                    <div class="modal-body">
                        <input type="hidden" name="gid" class="form-control" id="gid1"/>
                        商品名称：<label id="gname1" style="color: #0D47A1"></label>&nbsp;商品数量：<label id="gnum1" style="color: #0D47A1"></label><br>
                        商品价格：<label id="gprice1" style="color: #0D47A1"></label>&nbsp;商品详情：<label id="gdetail1" style="color: #0D47A1"></label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消
                        </button>
                        <button type="submit" class="btn btn-primary">
                            确认删除
                        </button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
</div>

<!-- jquery vendor -->
<script src="assets/js/lib/jquery.min.js"></script>
<script src="assets/js/lib/jquery.nanoscroller.min.js"></script>
<!-- nano scroller -->
<script src="assets/js/lib/menubar/sidebar.js"></script>
<script src="assets/js/lib/preloader/pace.min.js"></script>
<!-- sidebar -->

<!-- bootstrap -->

<!-- JS Grid Scripts Start-->
<script src="assets/js/lib/jsgrid/db.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.core.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.load-indicator.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.load-strategies.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.sort-strategies.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.field.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.text.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.number.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.select.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.checkbox.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.control.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid-init.js"></script>
<!-- JS Grid Scripts End-->

<script src="assets/js/lib/bootstrap.min.js"></script><script src="assets/js/scripts.js"></script>
<!-- scripit init-->

</body>
</html>
