<%--
  Created by IntelliJ IDEA.
  User: jm
  Date: 2020/4/5
  Time: 21:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>登录</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/lib/helper.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="assets/js/lib/jquery.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        var str="${msg}";
        if(str!=""){
            alert(str);
        }

        function show(){
            if($("#pwd").attr("type")=="password"){
                $("#pwd").attr("type","text");
                $("#open").attr("style","display:none");
                $("#close").attr("style","display:show");
            }else{
                $("#pwd").attr("type","password");
                $("#open").attr("style","display:show");
                $("#close").attr("style","display:none");
            }
        }
    </script>

</head>

<body class="bg-primary">

<div class="unix-login">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="login-content">
                    <div class="login-form">
                        <h2 style="text-align: center;color: #104E8B">Administratior Login</h2><br><br>
                        <form action="login" method="post">
                            <div class="form-group">
                                <input type="text" name="aname" class="form-control" placeholder="请输入用户名">
                            </div>
                            <div class="form-group">
                                <input type="password" name="pwd" id="pwd" class="form-control" placeholder="请输入密码">
                            </div>
                            <label style="margin-top: -40px;margin-left: 640px;">
                                <a onclick="show()"><span id="open" class="glyphicon glyphicon-eye-open"></span>
                                    <span id="close" class="glyphicon glyphicon-eye-close" style="display:none;"></span></a>
                            </label>
                            <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Sign in</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
