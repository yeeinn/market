<%--<jsp:useBean id="queryProvide" scope="request" type="com.sun.javafx.webkit.Accessor"/>--%>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>供应商管理</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="assets/css/lib/jsgrid/jsgrid-theme.min.css" rel="stylesheet" />
    <link href="assets/css/lib/jsgrid/jsgrid.min.css" type="text/css" rel="stylesheet" />
    <link href="assets/css/lib/menubar/sidebar.css" rel="stylesheet">
    <link href="assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/lib/helper.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css">
    <script  src="assets/js/sweetalert-dev.js"></script>
    <script src="assets/js/my97/WdatePicker.js"></script>

    <script type="text/javascript">
        function fenye(page) {
            var address=document.getElementById("address").value;
            var name=document.getElementById("name").value;
            var url='queryByProvide?page='+page;
            if(address!=0){
                url+="&address="+address;
            }
            if(name!=0){
                url+="&name="+name;
            }
            location=url;
        }
        function deleteByPid(pid) {
            if(confirm("确定要删除吗？")){
                $.get("deleteByPid/"+pid,function (data) {
                    if (data) {
                        alert("删除成功啦");
                    } else {
                        alert("删除失败啦");
                    }
                    window.location.reload();
                });
            }
        }
        function updateByPid() {
            $.post("updateByPid",$("#queryByPidform").serialize(),function(data){
                if(data){
                    alert("修改成功");
                }else{
                    alert("修改失败");
                }
                window.location.reload();
            });
        }
        function queryByPid(pid) {
            $.get("queryByPid/"+pid,function (data) {
                $("#pid").val(data.pid);
                $("#pname").val(data.pname);
                $("#ptelephone").val(data.ptelephone);
                $("#paddress").val(data.paddress);
            });
        }
    </script>


</head>

<body>
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <div class="logo"><a href="toindex"><!-- <img src="assets/images/logo.png" alt="" /> --><span>CRM</span></a></div>
            <ul>
                <li class="label">Goods</li>
                <li><a class="sidebar-sub-toggle"><i class="ti-home"></i> 供应商管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryByProvide">查看供应商信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-panel"></i> 进货管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryByPage">查看进货信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-layout-grid4-alt"></i> 库存管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="allgoods">查看库存</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-money"></i> 销售管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="querySell/1">查看销售信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-view-list-alt"></i> 退货管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryTHs">查看退货信息</a></li>
                    </ul>
                </li>
                <li class="label">Person</li>
                <li><a class="sidebar-sub-toggle"><i class="ti-user"></i>个人中心<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="toupdatePwd">修改密码</a></li>
                    </ul>
                </li>
                <li class="label">Exit</li>
                <li><a href="tologin"><i class="ti-close"></i> 退出系统 </a></li>
            </ul>
        </div>
    </div>
</div>
<!-- /# sidebar -->
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="float-left">
                    <div class="hamburger sidebar-toggle">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
                <div class="float-right">
                    <div>
                        <div class="header-icon">
                            <span class="user-avatar"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;${admin.aname}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-wrap">
    <div class="main">
        <div class="col-md-12">
            <div class="container-fluid">
                <div id="main-content">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-inline" action="queryByProvide"
                                  method="post">
                                <div class="form-group">
                                    <label>
                                        供应商
                                    </label>
                                    <input type="text" class="form-control" id="name"
                                           value="${queryProvide.name}" name="name">
                                </div>
                                <div class="form-group">
                                    <label>
                                        供应商地址
                                    </label>
                                    <input type="text" class="form-control" id="address"
                                           value="${queryProvide.address}" name="address">
                                </div>
                                <button type="submit" class="btn btn-primary" style="margin-top: 24px;">
                                    查询
                                </button>
                            </form>
                        </div>
                    </div>
                    <td><a href="addprovide"><h4>点击此处可添加供应商信息哦~</h4></a></td>
                    <table class="table-striped" style="width: 100%;">
                        <thead>
                        <tr style="height:48px;">
                            <th>供应商</th>
                            <th>供应商电话</th>
                            <th>供应商地址</th>
                            <th>操作选择</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${provideList}" var="p">
                            <tr style="height: 32px;">
                                <td>${p.pname}</td>
                                <td>${p.ptelephone}</td>
                                <td>${p.paddress}</td>
                                <td><a data-toggle="modal" data-target="#changeprovide"
                                        onclick="queryByPid(${p.pid})">修改</a></td>
                                <td><a  onclick="deleteByPid(${p.pid})">删除</a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <div align="center">
                        当前页数：${queryProvide.page }&nbsp;&nbsp;&nbsp;&nbsp;
                        <a onclick="fenye(1)">首页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a onclick="fenye(${queryProvide.next})">上一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a onclick="fenye(${queryProvide.last})">下一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        <a onclick="fenye(${queryProvide.pagecount})">尾页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        总页数：${queryProvide.pagecount }
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#wrapper -->
<div class="modal fade" id="changeprovide" tabindex="-1"
     role="dialog" aria-labelledby="ModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="ModalLabel">
                    修改供应商信息
                </h4>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="queryByPidform">
                    <input type="hidden" id="pid" name="pid" />
                    <div class="form-group">
                        <div class="form-group">
                            <label for="pname" class="col-sm-3 control-label">
                                供应商
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pname"
                                       placeholder="供应商" name="pname" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ptelephone" class="col-sm-3 control-label">
                                供应商电话
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="ptelephone"
                                       placeholder="供应商电话" name="ptelephone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="paddress" class="col-sm-3 control-label">
                                供应商地址
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="paddress"
                                       placeholder="供应商地址" name="paddress">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    关闭
                </button>
                <button type="button" class="btn btn-primary"
                        onclick="updateByPid()">
                    保存修改
                </button>
            </div>
        </div>
    </div>
</div>
<!-- jquery vendor -->
<script src="assets/js/jquery-3.3.1.js"></script>
<script src="assets/js/lib/jquery.min.js"></script>
<script src="assets/js/lib/jquery.nanoscroller.min.js"></script>
<!-- nano scroller -->
<script src="assets/js/lib/menubar/sidebar.js"></script>
<script src="assets/js/lib/preloader/pace.min.js"></script>
<!-- sidebar -->
<!-- jQuery -->
<%--<script src="<%=basePath%>assets/js/jquery.min.js"></script>--%>

<%--<!-- Bootstrap Core JavaScript -->--%>
<%--<script src="<%=basePath%>assets/js/bootstrap.min.js"></script>--%>

<%--<!-- Metis Menu Plugin JavaScript -->--%>
<%--<script src="<%=basePath%>assets/js/metisMenu.min.js"></script>--%>

<%--<!-- DataTables JavaScript -->--%>
<%--<script src="<%=basePath%>assets/js/jquery.dataTables.min.js"></script>--%>
<%--<script src="<%=basePath%>assets/js/dataTables.bootstrap.min.js"></script>--%>

<%--<!-- Custom Theme JavaScript -->--%>
<%--<script src="<%=basePath%>assets/js/sb-admin-2.js"></script>--%>
<!-- bootstrap -->

<!-- JS Grid Scripts Start-->
<script src="assets/js/lib/jsgrid/db.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.core.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.load-indicator.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.load-strategies.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.sort-strategies.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.field.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.text.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.number.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.select.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.checkbox.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.control.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid-init.js"></script>
<!-- JS Grid Scripts End-->
<script src="assets/js/lib/bootstrap.min.js"></script><script src="assets/js/scripts.js"></script>
<!-- scripit init-->
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>
