<%--
  Created by IntelliJ IDEA.
  User: jm
  Date: 2020/4/5
  Time: 22:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>退货管理</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="assets/css/lib/jsgrid/jsgrid-theme.min.css" rel="stylesheet" />
    <link href="assets/css/lib/jsgrid/jsgrid.min.css" type="text/css" rel="stylesheet" />
    <link href="assets/css/lib/menubar/sidebar.css" rel="stylesheet">
    <link href="assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/lib/helper.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css">
    <script  src="assets/js/sweetalert-dev.js"></script>
    <script src="assets/js/my97/WdatePicker.js"></script>


</head>

<body>
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <div class="logo"><a href="toindex"><!-- <img src="assets/images/logo.png" alt="" /> --><span>CRM</span></a></div>
            <ul>
                <li class="label">Goods</li>
                <li><a class="sidebar-sub-toggle"><i class="ti-home"></i> 供应商管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryByProvide">查看供应商信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-panel"></i> 进货管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryByPage">查看进货信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-layout-grid4-alt"></i> 库存管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="allgoods">查看库存</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-money"></i> 销售管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="querySell/1">查看销售信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-view-list-alt"></i> 退货管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryTHs">查看退货信息</a></li>
                    </ul>
                </li>
                <li class="label">Person</li>
                <li><a class="sidebar-sub-toggle"><i class="ti-user"></i>个人中心<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="toupdatePwd">修改密码</a></li>
                    </ul>
                </li>
                <li class="label">Exit</li>
                <li><a href="tologin"><i class="ti-close"></i> 退出系统 </a></li>
            </ul>
        </div>
    </div>
</div>
<!-- /# sidebar -->

<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="float-left">
                    <div class="hamburger sidebar-toggle">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
                <div class="float-right">
                    <div>
                        <div class="header-icon">
                            <span class="user-avatar"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;${admin.aname}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-wrap">
    <div class="main">
        <div class="col-md-12">
            <div class="container-fluid">
                <div id="main-content">
                    <form class="form-inline text-center">
                        <div class="form-group">
                            退货商品：<input type="text"  class="form-control" id="content"  name="content" value="${qt.content}" style="width:180px;"/>
                        </div>

                        <div class="form-group">
                            退货日期： <input type="text"  class="form-control" id="date"  name="date" value="${qt.date}" style="width:180px;" onfocus="WdatePicker()"/>
                        </div>
                        <button type="submit" class="btn btn-primary">
                            查询
                        </button>
                    </form>
                        <table class="table table-hover" style="width: 100%;">
                            <thead>
                            <tr style="height:48px;">
                                <th>退货商品</th>
                                <th>退货数量</th>
                                <th>退货日期</th>
                                <th>退货原因</th>
                                <th>
                                    <a data-toggle="modal" data-target="#addGoods">
                                        <span class="glyphicon glyphicon-pencil" style="color: #ADADAD"></span>
                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${ths}" var="t">
                                <tr style="height: 32px;">
                                    <td>${t.good.gname}</td>
                                    <td>${t.tnum}</td>
                                    <td>${t.tdate}</td>
                                    <td>${t.treason}</td>
                                    <td>
                                        <a data-toggle="modal" data-target="#queryTH" onclick="queryOne(${t.tid})">
                                            <span class="glyphicon glyphicon-edit" style="color: #ADADAD"></span>
                                        </a>&nbsp;&nbsp;&nbsp;
                                        <a onclick="delTH(${t.tid})">
                                            <span class="glyphicon glyphicon-trash" style="color: #ADADAD"></span>
                                        </a>&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                        <center>
                            每页显示: <select id="rows" name="rows" style="width: 50px">
                            <option value="5" <c:if test="${qt.rows==5}"> selected</c:if>>5</option>
                            <option value="10" <c:if test="${qt.rows==10}"> selected</c:if>>10</option>
                            <option value="15" <c:if test="${qt.rows==15}"> selected</c:if>>15</option>
                            <option value="20" <c:if test="${qt.rows==20}"> selected</c:if>>20</option>
                        </select> 条&nbsp;&nbsp; 共${qt.pageCount}页记录&nbsp;&nbsp;
                            <button type="button" onclick="fenye(1)">首页</button>
                            <button type="button" onclick="fenye(${qt.last})">上一页</button>
                            <input id="page" type="text" name="page"
                                   value="${qt.page}" style="width: 50px"></input>/${qt.pageCount}页
                            <button type="button" onclick="fenye(${qt.next})">下一页</button>
                            <button type="button" onclick="fenye(${qt.pageCount})">末页</button>
                        </center>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function fenye(page) {
        var rows=document.getElementById("rows").value;
        var content=document.getElementById("content").value;
        var date=document.getElementById("date").value;
        var url='queryTHs?page='+page;
        if(rows!=0){
            url+="&rows="+rows;
        }
        if(content!=''){
            url+="&content="+content;
        }
        if(date!=''){
            url+="&date="+date;
        }
        location=url;
    }
    function addTH() {
        $.post("addTH",$("#addTh").serialize(),function (data) {
            if(data){
                swal("提交成功","","success");
                setTimeout("location.href = 'http://" + location.host + "<%=path%>" + "/queryTHs'", 1000);
            }else {
                swal("提交失败","退货单号不存在或退货数量与售后数量不符","error");
            }
        });
    }

    function queryOne(id) {
        $.get("queryTH/"+id,function (data) {
            $("#tid").val(data.tid);
            $("#sid").val(data.sid);
            $("#tnum").val(data.tnum);
            $("#treason").val(data.treason);
            $("#tdate").val(data.tdate);
        });
    }

    function updTH() {
        $.post("updateTH",$("#editTH").serialize(),function (data) {
            if(data){
                swal("修改成功","","success");
                setTimeout("location.href = 'http://" + location.host + "<%=path%>" + "/queryTHs'", 1000);
            }else {
                swal("修改失败","退货数量与售后数量不符","error");
            }
        });
    }

    function delTH(id) {
        swal({
                title: "确定取消退货吗？",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定",
                cancelButtonText: "取消",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.get("delTH/"+id, function (data) {
                        if (data) {
                            swal("取消成功","","success");
                            setTimeout("location.href = 'http://" + location.host + "<%=path%>" + "/queryTHs'", 1000);
                        } else {
                            swal("取消失败","","error");
                            setTimeout("location.href = 'http://" + location.host + "<%=path%>" + "/queryTHs'", 1000);
                        }
                    })

                } else {
                    swal("取消删除");
                }
            })
    }


</script>
    <!--添加退货商品模态框（Modal） -->
    <div class="modal fade" id="addGoods" tabindex="-1" role="dialog" aria-labelledby="addGoodsLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="addGoodsLabel">
                        退货商品
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="addTh">
                    <div class="modal-body">

                        <div class="form-group">
                            购买信息: <select class="form-control" style=" padding: 0px 20px 0 27px;" name="sid">
                                <c:forEach items="${sells}" var="s">
                                    <option value="${s.sid}">
                                            购买商品：${s.good.gname}，购买数量：${s.snum}
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group">
                            退货数量:<input type="text" name="tnum" class="form-control fc-day-number" placeholder="退货数量"/>
                        </div>
                        <div class="form-group">
                            退货原因:<input type="text" name="treason" class="form-control" placeholder="退货原因"/>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消
                    </button>
                    <button type="submit" class="btn btn-primary" onclick="addTH()">
                        提交
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
<!--修改退货商品模态框（Modal） -->
<div class="modal fade" id="queryTH" tabindex="-1" role="dialog" aria-labelledby="queryTHLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="queryTHLabel">
                    修改退货信息
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editTH">
                <div class="modal-body">
                    <input type="hidden" id="tid" name="tid"/>
                    <div class="form-group">
                        退货单号:<input type="text" id="sid"  name="sid" class="form-control" placeholder="退货单号" readonly="readonly"/>
                    </div>
                    <div class="form-group">
                        退货数量:<input type="text" id="tnum" name="tnum" class="form-control fc-day-number" placeholder="退货数量" readonly="readonly"/>
                    </div>
                    <div class="form-group">
                        退货日期:<input type="text" id="tdate" name="tdate" class="form-control fc-day-number" placeholder="退货日期" readonly="readonly"/>
                    </div>
                    <div class="form-group">
                        退货原因:<input type="text" id="treason" name="treason" class="form-control" placeholder="退货原因"/>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消
                </button>
                <button type="submit" class="btn btn-primary" onclick="updTH()">
                    提交
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

    <!-- jquery vendor -->
<script src="assets/js/lib/jquery.min.js"></script>
<script src="assets/js/lib/jquery.nanoscroller.min.js"></script>
<!-- nano scroller -->
<script src="assets/js/lib/menubar/sidebar.js"></script>
<script src="assets/js/lib/preloader/pace.min.js"></script>
<!-- sidebar -->

<!-- bootstrap -->

<!-- JS Grid Scripts Start-->
<script src="assets/js/lib/jsgrid/db.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.core.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.load-indicator.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.load-strategies.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.sort-strategies.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.field.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.text.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.number.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.select.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.checkbox.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.control.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid-init.js"></script>
<!-- JS Grid Scripts End-->

<script src="assets/js/lib/bootstrap.min.js"></script><script src="assets/js/scripts.js"></script>
<!-- scripit init-->

<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>
