<%--
  Created by IntelliJ IDEA.
  User: zjx
  Date: 2020/4/3
  Time: 17:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>进货管理</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="assets/css/lib/data-table/buttons.bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/lib/menubar/sidebar.css" rel="stylesheet">
    <link href="assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/lib/helper.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script src="assets/js/sweetalert-dev.js"></script>
    <script src="assets/js/my97/WdatePicker.js"></script>




</head>

<body>
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <div class="logo"><a href="toindex"><!-- <img src="assets/images/logo.png" alt="" /> --><span>CRM</span></a></div>
            <ul>
                <li class="label">Goods</li>
                <li><a class="sidebar-sub-toggle"><i class="ti-home"></i> 供应商管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryByProvide">查看供应商信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-panel"></i> 进货管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryByPage">查看进货信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-layout-grid4-alt"></i> 库存管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="allgoods">查看库存</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-money"></i> 销售管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="querySell/1">查看销售信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-view-list-alt"></i> 退货管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryTHs">查看退货信息</a></li>
                    </ul>
                </li>
                <li class="label">Person</li>
                <li><a class="sidebar-sub-toggle"><i class="ti-user"></i>个人中心<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="toupdatePwd">修改密码</a></li>
                    </ul>
                </li>
                <li class="label">Exit</li>
                <li><a href="tologin"><i class="ti-close"></i> 退出系统 </a></li>
            </ul>
        </div>
    </div>
</div>
<!-- /# sidebar -->

<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="float-left">
                    <div class="hamburger sidebar-toggle">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
                <div class="float-right">
                    <div>
                        <div class="header-icon">
                            <span class="user-avatar"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;${admin.aname}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-wrap">
    <div class="main">
        <div class="col-md-12">
            <div class="container-fluid">
                <div id="main-content">
                    <form class="form-inline">
                        <div class="form-group">
                            进货商品：<input type="text"  class="form-control" id="gname"  name="gname" value="${qj.gname}" style="width:180px;"/>
                        </div>

                        <div class="form-group">
                            进货日期： <input type="text"  class="form-control" id="jdate"  name="jdate" value="${qj.jdate}" style="width:180px;" onfocus="WdatePicker()"/>
                        </div>
                        <button type="submit" class="btn btn-primary">
                            查询
                        </button>
                    </form>
                    <table class="table-striped" style="width: 100%;">
                                        <thead>
                                        <tr style="height:48px;">
                                            <th>进货id</th>
                                            <th>商品名</th>
                                            <th>供应商名</th>
                                            <th>进货数量</th>
                                            <th>进货时间</th>
                                            <th>进货单价</th>
                                            <th>进货总价</th>
                                            <th>备注信息</th>
                                            <th><a  class="btn btn-primary"
                                                    data-toggle="modal" data-target="#JinhuoAddDialog" >添加进货信息</a></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach items="${jList}" var="j">
                                        <tr style="height:32px;">
                                            <td>${j.jid}</td>
                                            <td>${j.goods.gname}</td>
                                            <td>${j.provide.pname}</td>
                                            <td>${j.jnum}</td>
                                            <td>${j.jdate}</td>
                                            <td>${j.jprice}</td>
                                            <td>${j.jtotal}</td>
                                            <td>${j.jmsg}</td>
                                            <td>
                                                <a   data-toggle="modal" data-target="#JinhuoUpdateDialog" onclick="editJinhuo(${j.jid})">
                                                    <span class="glyphicon glyphicon-edit" style="color: #ADADAD"></span>
                                                </a>&nbsp;&nbsp;&nbsp;
                                                <a onclick="deleteJinhuo(${j.jid})">
                                                    <span class="glyphicon glyphicon-trash" style="color: #ADADAD"></span>
                                                </a>&nbsp;&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                    <div align="center">
                            当前页数：${qj.page}&nbsp;&nbsp;&nbsp;&nbsp;
                            <a onclick="fenye(1)">首页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a onclick="fenye(${qj.last})">上一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a onclick="fenye(${qj.next})">下一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a onclick="fenye(${qj.pageCount})">尾页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            总页数：${qj.pageCount}
                    </div>
                </div>
                            </div>
                        </div>
                    </div>
                </div>

<!--进货信息添加-->
<div class="modal fade" id="JinhuoAddDialog" tabindex="-1"
     role="dialog" aria-labelledby="addModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="addModalLabel">
                    添加进货信息
                </h4>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="add_jinhuo_form">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            商品名
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="add_gname" name="gname">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            供应商名
                        </label>
                        <div class="col-sm-10">
<%--                            <input type="text" class="form-control" id="add_pname" name="pname">--%>
                            <select id="add_pid" name="pid" style="height: 36px">
                                <c:forEach items="${provides}" var="p">
                                    <option value="${p.pid}">${p.pname}</option>
                                </c:forEach>
                            </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            数量
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="add_jnum" name="jnum">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            时间
                        </label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="add_jdate" name="jdate">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            单价
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="add_jprice" name="jprice">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            总价
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="add_jtotal" name="jtotal">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            备注
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="add_jmsg" name="jmsg">
                        </div>
                    </div>

                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    关闭
                </button>
                <button type="button" class="btn btn-primary" onclick="addJinhuo()">
                    确定添加
                </button>
            </div>
        </div>
    </div>
</div>


<!--进货信息修改-->
<div class="modal fade" id="JinhuoUpdateDialog" tabindex="-1"
     role="dialog" aria-labelledby="updateModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="updateModalLabel">
                    修改进货信息
                </h4>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <form class="form-horizontal" id="edit_jinhuo_form">
                    <div class="form-group" hidden="hidden">
                        <label class="col-sm-2 control-label">
                            销售信息ID
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit_jid" name="jid" readonly="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            商品名
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit_gname" name="gname" readonly="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            数量
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit_jnum" name="jnum" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            单价
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit_jprice" name="jprice">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            总价
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit_jtotal" name="jtotal">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            备注
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="edit_jmsg" name="jmsg">
                        </div>
                    </div>

                </form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    关闭
                </button>
                <button type="button" class="btn btn-primary"  onclick="updateJinhuo()">
                    确定修改
                </button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    function fenye(page) {
        var gname = document.getElementById("gname").value;
        var jdate = document.getElementById("jdate").value;
        var url='queryByPage?page='+page;
        if (gname!=''){
            url+="&gname="+gname;
        }
        if (jdate!=''){
            url+="&jdate="+jdate;
        }
        location=url;
    }


    function deleteJinhuo(jid) {
        if (confirm("确定删除？")){
            //alert(jid);
            $.post("delete",{"jid":jid},function (data) {
                if (data) {
                    alert("删除成功");
                }else {
                    alert("删除失败");
                }
                window.location.reload();
            });
        }
    }


    function addJinhuo() {
        // var jinhuo = $("#add_jinhuo_form").serialize();
        // alert(jinhuo);
        $.post("add",$("#add_jinhuo_form").serialize(),function (data) {

            if (data) {
                alert("添加成功");
            }else {
                alert("添加失败");
            }
            window.location.reload();
        });
    }


    function editJinhuo(jid) {
        $.post("edit",{"jid":jid},function (data) {
            $("#edit_jid").val(data.jid);
            $("#edit_gname").val(data.gname);
            $("#edit_jnum").val(data.jnum);
            $("#edit_jprice").val(data.jprice);
            $("#edit_jtotal").val(data.jtotal);
            $("#edit_jmsg").val(data.jmsg);
        });
    }

    function updateJinhuo() {
        $.post("update",$("#edit_jinhuo_form").serialize(),function (data) {
            if (data) {
                alert("修改成功");
            }else {
                alert("修改失败");
            }
            window.location.reload();
        });
    }

</script>



<!-- jquery vendor -->
<script src="assets/js/lib/jquery.min.js"></script>
<script src="assets/js/lib/jquery.nanoscroller.min.js"></script>
<!-- nano scroller -->
<script src="assets/js/lib/menubar/sidebar.js"></script>
<script src="assets/js/lib/preloader/pace.min.js"></script>
<!-- sidebar -->

<!-- bootstrap -->

<script src="assets/js/lib/bootstrap.min.js"></script><script src="assets/js/scripts.js"></script>
<!-- scripit init-->
<script src="assets/js/lib/data-table/datatables.min.js"></script>
<script src="assets/js/lib/data-table/buttons.dataTables.min.js"></script>
<script src="assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="assets/js/lib/data-table/buttons.flash.min.js"></script>
<script src="assets/js/lib/data-table/jszip.min.js"></script>
<script src="assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="assets/js/lib/data-table/datatables-init.js"></script>

<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>


</body>
</html>
