<%--
  Created by IntelliJ IDEA.
  User: jm
  Date: 2020/4/5
  Time: 22:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="C" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>库存管理</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="assets/css/lib/jsgrid/jsgrid-theme.min.css" rel="stylesheet"/>
    <link href="assets/css/lib/jsgrid/jsgrid.min.css" type="text/css" rel="stylesheet"/>
    <link href="assets/css/lib/menubar/sidebar.css" rel="stylesheet">
    <link href="assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/lib/helper.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript">


        function getSell(id) {
            $.post("querySellByID", {"sid": id}, function (data) {
               // alert(sid)
                if (data) {
                    $("#up_sid").val(id);
                    $("#up_gname").val(data.good.gname);
                    $("#up_snum").val(data.snum);
                    $("#up_sprice").val(data.sprice)
                    $("#up_smsg").val(data.smsg);
                }
            })
        }

        function deleteSell(id) {
            if (confirm("确认删除本条销售信息?")) {
                $.post("deleteSell", {"sid": id}, function (data) {

                        alert(data);
                        window.location = 'querySell/'+'${currentpage}';

                })
            }
        }
    </script>

</head>

<body>
<script type="text/javascript">
    var str="${msg}";
    if(str!=""){
        alert(str);
    }
</script>
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <div class="logo"><a href="toindex"><!-- <img src="assets/images/logo.png" alt="" /> --><span>CRM</span></a></div>
            <ul>
                <li class="label">Goods</li>
                <li><a class="sidebar-sub-toggle"><i class="ti-home"></i> 供应商管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryByProvide">查看供应商信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-panel"></i> 进货管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryByPage">查看进货信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-layout-grid4-alt"></i> 库存管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="allgoods">查看库存</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-money"></i> 销售管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="querySell/1">查看销售信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle"><i class="ti-view-list-alt"></i> 退货管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryTHs">查看退货信息</a></li>
                    </ul>
                </li>
                <li class="label">Person</li>
                <li><a class="sidebar-sub-toggle"><i class="ti-user"></i>个人中心<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="toupdatePwd">修改密码</a></li>
                    </ul>
                </li>
                <li class="label">Exit</li>
                <li><a href="tologin"><i class="ti-close"></i> 退出系统 </a></li>
            </ul>
        </div>
    </div>
</div>
<!-- /# sidebar -->

<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="float-left">
                    <div class="hamburger sidebar-toggle">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
                <div class="float-right">
                    <div>
                        <div class="header-icon">
                            <span class="user-avatar"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;${admin.aname}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-wrap">
    <div class="main">
        <div class="col-md-12">
            <div class="container-fluid">
                <div id="main-content">
                    <form class="form-group" action="querySell/1" method="post">

                            <input type="text" name="good.gname" class="form-control col-md-3" placeholder="商品名称" style="margin-top: 22px;"/>&nbsp;&nbsp;

                            <input type="text" name="sdate" class="form-control  col-md-3" placeholder="销售日期" style="margin-top: 22px;"/><br>&nbsp;&nbsp;


                        <input type="submit" class="btn btn-default" value="查询">
                         <a class="btn btn-primary" data-toggle="modal" data-target="#addSellDialog" >添加</a>
                    </form>
                    <table class="table-striped" style="width: 100%;text-align: center;">
                        <thead>
                        <tr style="height:48px;text-align: center;">
                            <th>销售编号</th>
                            <th>商品名</th>
                            <th>销售数量</th>
                            <th>商品原价</th>
                            <th>商品售价</th>
                            <th>总价</th>
                            <th>销售日期</th>
                            <th>备注</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${selllist}" var="s">
                            <tr style="height: 32px;">
                                <td>${s.sid}</td>
                                <td>${s.good.gname}</td>
                                <td>${s.snum}</td>
                                <td>${s.good.gprice}</td>
                                <td>${s.sprice}</td>
                                <td>${s.stotal}</td>
                                <td>${s.sdate}</td>
                                <td>${s.smsg}</td>
                                <td><a onclick="getSell(${s.sid})" data-toggle="modal" data-target="#modSells"><span
                                        class="glyphicon glyphicon-edit" style="color: #ADADAD"></span></a></td>
                                <td><a onclick="deleteSell(${s.sid})"><span class="glyphicon glyphicon-trash"
                                                                            style="color: #ADADAD"></span></a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <ul >

                    当前页数：<span >${currentpage}</span>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onclick="fenye('first')">首页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onclick="fenye('previous')">上一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onclick="fenye('next')">下一页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onclick="fenye('pagecount')">尾页</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    总页数：<span >${pagecount} </span>

                </ul> <!-- /.pagination -->
            </div>
        </div>
    </div>


    <!--添加商品信息模态框（Modal） -->
    <div class="modal fade" id="addSellDialog" tabindex="-1" role="dialog" aria-labelledby="addGoodsLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="addGoodsLabel">
                        添加销售记录
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">单条添加</a></li>
                        <li><a href="#adds" data-toggle="tab">批量添加</a></li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home">
                            <form action="addSell/${currentpage}" onsubmit="return validate()" method="post">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">商品名：</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" style="height: 32px;"  name="good.gid" id="add_gname">
                                            <option value="">----请选择----</option>
                                            <c:forEach items="${goods}" var="g">
                                                <option value="${g.gid}">${g.gname}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">商品数量：</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="snum" id="add_snum" class="form-control" placeholder="商品数量"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">商品单价：</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="sprice" class="form-control" placeholder="商品单价(若不填则默认使用预设价格)"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">备注：</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="smsg" class="form-control" placeholder="备注"/><br>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">取消
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        添加
                                    </button>
                                </div>
                            </form>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="adds">
                                <div class="form-group">
                                    <a href="download" class="btn btn-primary">下载模板文件</a>

                                </div>
                                <div class="modal-footer">
                                    <label class="col-sm-3 control-label">上传文件：</label>
                                    <form class="form-horizontal" action="uploadSellfile" enctype="multipart/form-data" method="post">
                                        <input type="file" class="form-control" name="sellfile">
                                        <button type="submit" class="btn btn-primary">
                                            添加
                                        </button>

                                    </form>
                                </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>

    <!--修改商品信息模态框（Modal） -->
    <div class="modal fade" id="modSells" tabindex="-1" role="dialog" aria-labelledby="modGoodsLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modGoodsLabel">
                        修改销售信息
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                </div>
                <form action="updateSell/${currentpage}"  method="post">
                    <div class="modal-body">
                        <input type="hidden" name="sid" class="form-control" id="up_sid"/>
                        商品名：<input type="text"  class="form-control" id="up_gname" readonly="readonly"/><br>
                        商品数量： <input type="text" name="snum" class="form-control" id="up_snum" placeholder="商品数量" readonly="readonly"/><br>
                        商品价格： <input type="text" name="sprice" class="form-control" id="up_sprice" placeholder="商品价格"/><br>
                        备注：<input type="text" name="smsg" class="form-control" id="up_smsg" placeholder="备注"/><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消
                        </button>
                        <button type="submit" class="btn btn-primary">
                            修改
                        </button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->
    </div>
</div>

<!-- jquery vendor -->
<script src="assets/js/lib/jquery.min.js"></script>
<script src="assets/js/lib/jquery.nanoscroller.min.js"></script>
<!-- nano scroller -->
<script src="assets/js/lib/menubar/sidebar.js"></script>
<script src="assets/js/lib/preloader/pace.min.js"></script>
<!-- sidebar -->

<!-- bootstrap -->

<!-- JS Grid Scripts Start-->
<script src="assets/js/lib/jsgrid/db.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.core.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.load-indicator.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.load-strategies.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.sort-strategies.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.field.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.text.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.number.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.select.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.checkbox.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.control.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid-init.js"></script>
<!-- JS Grid Scripts End-->

<script src="assets/js/lib/bootstrap.min.js"></script>
<script src="assets/js/scripts.js"></script>
<!-- scripit init-->
<script type="text/javascript">
    function fenye(state){
        var url = "querySell/";
        var page = parseInt([[${currentpage}]]);
        var pagecount =  parseInt([[${pagecount}]]);
        if(state=='first'){
            url = url+'1';
        }else if(state=='previous'){
            if(page==1){
                url=url+'1';
            }
            else{
                url = url+ (page-1);
            }
        }else if(state=='next'){
            if(page==pagecount){
                url=url+pagecount;
            }
            else{
                url = url+(page+1);
            }

        }else if(state=='pagecount'){
            url = url + pagecount;
        }

        location = url;

    }
    function validate() {
        var gname = $("#add_gname").val();
        var gnum = $("#add_snum").val();
        if(gname==""){
            alert("请选择商品名！");
            return false;
        }
        if(gnum==""){
            alert("请填写销售商品数量！");
            return false;
        }
        return true;
    }


</script>

</body>
</html>
