<%--
 Created by IntelliJ IDEA.
  User: skj
  Date: 2020/4/8
  Time: 18:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>添加供应商</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

    <!-- Styles -->
    <link href="assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="assets/css/lib/jsgrid/jsgrid-theme.min.css" rel="stylesheet" />
    <link href="assets/css/lib/jsgrid/jsgrid.min.css" type="text/css" rel="stylesheet" />
    <link href="assets/css/lib/menubar/sidebar.css" rel="stylesheet">
    <link href="assets/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/lib/helper.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <meta charset="utf-8">

    <link href="assets/css/styleee.css" rel="stylesheet" type='text/css'>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <script src="assets/js/jquery-3.3.1.js"></script>
    <script type="text/javascript">
        var msg='${msg}';
        if(msg!=''){
            alert(msg);
        }
    </script>
</head>

<body>
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <div class="logo"><a href="toindex"><!-- <img src="assets/images/logo.png" alt="" /> --><span>CRM</span></a></div>
            <ul>
                <li class="label">Goods</li>
                <li><a class="sidebar-sub-toggle" style="font-size: 14px;"><i class="ti-home"></i> 供应商管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryByProvide" style="font-size: 14px;">查看供应商信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle" style="font-size: 14px;"><i class="ti-panel"></i> 进货管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryByPage" style="font-size: 14px;">查看进货信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle" style="font-size: 14px;"><i class="ti-layout-grid4-alt"></i> 库存管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="allgoods" style="font-size: 14px;">查看库存</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle" style="font-size: 14px;"><i class="ti-money"></i> 销售管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="querySell/1" style="font-size: 14px;">查看销售信息</a></li>
                    </ul>
                </li>
                <li><a class="sidebar-sub-toggle" style="font-size: 14px;"><i class="ti-view-list-alt"></i> 退货管理 <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="queryTHs" style="font-size: 14px;">查看退货信息</a></li>
                    </ul>
                </li>
                <li class="label">Person</li>
                <li><a class="sidebar-sub-toggle" style="font-size: 14px;"><i class="ti-user"></i>个人中心<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="toupdatePwd" style="font-size: 14px;">修改密码</a></li>
                    </ul>
                </li>
                <li class="label">Exit</li>
                <li><a href="tologin" style="font-size: 14px;"><i class="ti-close"></i> 退出系统 </a></li>
            </ul>
        </div>
    </div>
</div>
<!-- /# sidebar -->
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="float-right">
                    <div>
                        <div class="header-icon">
                            <span class="user-avatar"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;${admin.aname}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-wrap">
<!-----start-main---->
<div class="main">
    <div class="login-form">
        <h1>请添加供应商信息ヾ(^∀^)ﾉ</h1>
        <div class="head">
            <img src="assets/images/user.png" alt=""/>
        </div>
        <form action="addprovide" method="post">
            <input type="text" class="text" name="pname" value="请输入供应商名称" onfocus="this.value = '';"
                   onblur="if (this.value == '') {this.value = '请输入供应商名称';}">
            <input type="text" class="text" name="ptelephone" value="请输入供应商电话" onfocus="this.value = '';"
                   onblur="if (this.value == '') {this.value = '请输入供应商电话';}">
            <input type="text" class="text" name="paddress" value="请输入供应商地址" onfocus="this.value = '';"
                   onblur="if (this.value == '') {this.value = '请输入供应商地址';}">
            <div class="submit">
                <input type="submit"  value="确认添加" >
            </div>
        </form>
    </div>
    <!--//End-login-form-->
    <!-----start-copyright---->
    <!-----//end-copyright---->
</div>
<!-----//end-main---->
</div>

<div style="display:none"><script src='http://v7.cnzz.com/stat.php?id=155540&web_id=155540' language='JavaScript' charset='gb2312'></script></div>

<!-- jquery vendor -->
<script src="assets/js/lib/jquery.min.js"></script>
<script src="assets/js/lib/jquery.nanoscroller.min.js"></script>
<!-- nano scroller -->
<script src="assets/js/lib/menubar/sidebar.js"></script>
<script src="assets/js/lib/preloader/pace.min.js"></script>
<!-- sidebar -->

<!-- bootstrap -->

<!-- JS Grid Scripts Start-->
<script src="assets/js/lib/jsgrid/db.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.core.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.load-indicator.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.load-strategies.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.sort-strategies.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid.field.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.text.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.number.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.select.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.checkbox.js"></script>
<script src="assets/js/lib/jsgrid/fields/jsgrid.field.control.js"></script>
<script src="assets/js/lib/jsgrid/jsgrid-init.js"></script>
<!-- JS Grid Scripts End-->

<script src="assets/js/lib/bootstrap.min.js"></script><script src="assets/js/scripts.js"></script>
<!-- scripit init-->

</body>
</html>