package com.crm.feigns;

import com.crm.entity.Sell;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@FeignClient(name = "marketserver9001")
public interface SellFeign {
    @PostMapping("addsell")
    public boolean addSell(@RequestBody Sell sell);

    @RequestMapping("queryAll/{page}")
    public List<Sell> queryAll(@RequestBody Sell sell,@RequestParam @PathVariable int page);



    @RequestMapping("updatesell")
    public boolean updateSell(@RequestBody Sell sell);

    @RequestMapping("deletesell")
    public boolean deleteSell(@RequestParam int sid);

    @RequestMapping("pagecount")
    public int pagecount(@RequestBody Sell sell);

    @RequestMapping("querySellByID")
    public Sell querySellByID(@RequestParam int sid);

@RequestMapping("addsells")
    public int add(@RequestBody List<Sell> sells);

}
