package com.crm.feigns;

import com.crm.entity.Jinhuo;
import com.crm.entity.QueryJinhuo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(name = "marketserver9001")
public interface JinhuoFeign {

    @RequestMapping("Jinhuo/query")
    public List<Jinhuo> query();

    @RequestMapping("Jinhuo/queryByPage")
    public Map<String,Object> queryByPage(@RequestBody QueryJinhuo queryJinhuo);

    @RequestMapping("Jinhuo/delete")
    public boolean delete(@RequestParam int jid);

    @RequestMapping("Jinhuo/edit")
    public Jinhuo edit(@RequestParam int jid);

    @RequestMapping("Jinhuo/update")
    public boolean update(@RequestBody Jinhuo jinhuo);

    @RequestMapping("Jinhuo/add")
    public boolean add(@RequestBody Jinhuo jinhuo);


}
