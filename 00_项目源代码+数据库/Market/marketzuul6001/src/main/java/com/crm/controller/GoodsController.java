package com.crm.controller;

import com.crm.entity.Goods;
import com.crm.feigns.GoodsFeign;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: jm
 * \* Date: 2020/4/6
 * \* Time: 21:53
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
public class GoodsController {
    @Resource
    private GoodsFeign goodsFeign;

    @RequestMapping("allgoods")
    public String findAll(Model model){
        Map<String,Object> map=goodsFeign.findAll();
        List<Goods> goodsList =(List<Goods>) map.get("goods");
        model.addAttribute("goods",goodsList);
        return "goods";
    }

    @RequestMapping("addGoods")
    public String addGoods(Goods goods,Model model){
        String gname=goods.getGname();
        if(gname==""||gname==null){
            model.addAttribute("msg","请输入需要添加的商品名称");
            return "forward:/allgoods";
        }
        Map<String,Object> map=goodsFeign.addGoods(goods);
        //System.out.println("6001------------------"+map.get(("msg")));
        if("error".equals(map.get("msg"))){
            model.addAttribute("msg","该商品已存在");
            return "forward:/allgoods";
        }
        model.addAttribute("msg","添加成功");
        return "forward:/allgoods";
    }

    @RequestMapping("findById")
    @ResponseBody
    public List<Goods> finfById(Integer gid,Model model){
        //System.out.println("6001-------------gid"+gid);
        Map<String,Object> map=goodsFeign.findById(gid);
        List<Goods> good=(List<Goods>) map.get("good");
        //System.out.println("6001------------good"+good);
        return good;
    }

    @RequestMapping("modGoods")
    public String modGoods(Goods goods, Model model){
        goodsFeign.modGoods(goods);
        model.addAttribute("msg","修改成功");
        return "forward:/allgoods";
    }

    @RequestMapping("deleteGoods")
    public String deleteGoods(Goods goods,Model model){
        Integer gid=goods.getGid();
        //System.out.println("6001----------"+gid);
        goodsFeign.deleteGoods(gid);
        model.addAttribute("msg","删除成功");
        return "forward:/allgoods";
    }
}
