package com.crm.controller;

import com.crm.entity.Admin;
import com.crm.feigns.AdminFeign;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: jm
 * \* Date: 2020/4/5
 * \* Time: 22:03
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
public class AdminController {
    @Resource
    private AdminFeign adminFeign;

    @RequestMapping("tologin")
    public String tologin(){
        return "login";
    }

    @RequestMapping("login")
    public String login(String aname, String pwd, HttpSession session, Model model){
        if(aname==""||pwd==""){
            model.addAttribute("msg","请先输入用户名和密码");
            return "login";
        }
        Map<String,Object> map=adminFeign.login(aname,pwd);
        if("success".equals(map.get("msg"))){
            session.setAttribute("admin",map.get("admin"));
            return "index";
        }else{
            model.addAttribute("msg","用户名或密码错误");
            return "login";
        }
    }

    @RequestMapping("toupdatePwd")
    public String toupdatePwd(){
        return "updatepwd";
    }

    @RequestMapping("updatePwd")
    public String updatePwd(Admin admin,Model model){
        //System.out.println("6001---------aid"+admin.getAid());
        String pwd=admin.getPwd();
        if(pwd==""||pwd==null){
            model.addAttribute("msg","密码不能为空");
            return "updatepwd";
        }
        //System.out.println("6001-----------"+admin.getPwd());
        adminFeign.updatePwd(admin);
        model.addAttribute("msg","修改成功，请重新登录");
        return "forward:/tologin";
    }

    @RequestMapping("toindex")
    public String toindex(){
        return "index";
    }
}
