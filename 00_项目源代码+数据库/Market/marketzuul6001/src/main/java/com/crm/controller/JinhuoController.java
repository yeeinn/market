package com.crm.controller;

import com.crm.entity.Jinhuo;
import com.crm.entity.Provide;
import com.crm.entity.QueryJinhuo;
import com.crm.feigns.JinhuoFeign;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zjx
 * \* Date: 2020/4/6
 * \* Time: 22:49
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
public class JinhuoController {

    @Resource
    private JinhuoFeign jinhuoFeign;

    @RequestMapping("query")
    public String query(Model model){
        List<Jinhuo> jinhuoList = jinhuoFeign.query();
        model.addAttribute("jList",jinhuoList);
        return"jinhuo";
    }

    @RequestMapping("queryByPage")
    public String queryByPage(QueryJinhuo queryJinhuo,Model model){
        Map<String,Object> map = jinhuoFeign.queryByPage(queryJinhuo);
        List<Jinhuo> jinhuoList = (List<Jinhuo>) map.get("jinhuoList");
        List<Provide> provideList = (List<Provide>) map.get("provides");
        model.addAttribute("jList",jinhuoList);
        model.addAttribute("qj",map.get("queryJinhuo"));
        model.addAttribute("provides",provideList);
        return "jinhuo";

    }

    @RequestMapping("delete")
    @ResponseBody
    public boolean delete(@RequestParam int jid, HttpSession session){
        session.setAttribute("jid",jid);
        System.out.println(jid);
        return jinhuoFeign.delete(jid);
    }

    @RequestMapping("edit")
    @ResponseBody
    public Jinhuo edit(@RequestParam int jid,HttpSession session){
        session.setAttribute("jid",jid);
        System.out.println(jid);
        return jinhuoFeign.edit(jid);
    }

    @RequestMapping("update")
    @ResponseBody
    public boolean update(Jinhuo jinhuo, HttpSession session){
        session.setAttribute("jinhuo",jinhuo);
        return jinhuoFeign.update(jinhuo);
    }


    @RequestMapping("add")
    @ResponseBody
    public boolean add(Jinhuo jinhuo, HttpSession session){
        session.setAttribute("jinhuo",jinhuo);
        System.out.println("jinhuo"+jinhuo);
        return jinhuoFeign.add(jinhuo);
    }

}

