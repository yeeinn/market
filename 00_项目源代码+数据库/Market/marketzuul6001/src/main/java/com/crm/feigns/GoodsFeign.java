package com.crm.feigns;

import com.crm.entity.Goods;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "marketserver8001")
public interface GoodsFeign {
    @GetMapping("allgoods")
    public Map<String,Object> findAll();
    @PostMapping("addGoods")
    public Map<String,Object> addGoods(@RequestBody Goods goods);
    @GetMapping("findById")
    public Map<String,Object> findById(@RequestParam Integer gid);
    @PostMapping("modGoods")
    public Map<String,Object> modGoods(@RequestBody Goods goods);
    @PostMapping("deleteGoods")
    public Map<String,Object> deleteGoods(@RequestParam Integer gid);
}
