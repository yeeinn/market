package com.crm.controller;


import com.crm.entity.Goods;
import com.crm.entity.Sell;
import com.crm.feigns.GoodsFeign;
import com.crm.feigns.SellFeign;
import com.crm.util.ExcelUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lyy
 * \* Date: 2020/4/7
 * \* Time: 14:16
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
public class SellsController {
    @Resource
    private SellFeign sellFeign;
    @Resource
    private GoodsFeign goodsFeign;


    @RequestMapping("/querySell/{page}")
    public String  queryAll(Model model, Sell sell, @PathVariable int page){

        model.addAttribute("selllist",sellFeign.queryAll(sell,page));
        model.addAttribute("currentpage",page);
        model.addAttribute("pagecount",sellFeign.pagecount(sell));
        Map<String,Object> map=goodsFeign.findAll();
        List<Goods> goodsList =(List<Goods>) map.get("goods");
        model.addAttribute("goods",goodsList);
        return "Sell";
    }

    @RequestMapping("/addSell/{page}")
    public String addSell(Sell sell,Model model, @PathVariable int page){

        System.out.println(sell.getGood().getGid());
        if("".equals(sell.getSmsg())||null==sell.getSmsg()){
            sell.setSmsg("无");
        }
        if(sellFeign.addSell(sell)){
            model.addAttribute("msg","添加成功") ;
        }
        else{
            model.addAttribute("msg","添加失败") ;

        }
        //"redirect:querySell/1"
        return queryAll(model,sell,page);
    }

    @RequestMapping("/updateSell/{page}")

    public String updateSell(Sell sell,Model model,@PathVariable int page){
        sell.setStotal(sell.getSprice()*sell.getSnum());
        if("".equals(sell.getSmsg())||null==sell.getSmsg()){
            sell.setSmsg("无");
        }
        if(sellFeign.updateSell(sell)){
            model.addAttribute("修改成功");
            return queryAll(model,sell,page);
        }
        model.addAttribute("修改失败");
        return queryAll(model,sell,page);
    }

    @RequestMapping("/deleteSell")
    @ResponseBody
    public String deleteSell(int sid){
        if(sellFeign.deleteSell(sid)){
            return "删除成功";
        }
        return "删除失败";
    }

    @RequestMapping("/querySellByID")
    @ResponseBody
    public Sell querySellByID(int sid){
        return sellFeign.querySellByID(sid);
    }

    //下载模板文件
    @RequestMapping("download")

    public void downloadmoban(HttpSession session,
                              HttpServletResponse response) throws IOException {
        String path = session.getServletContext().getRealPath("/sellmoban.xls");
        File file = new File(path);
        BufferedInputStream br = new BufferedInputStream(new FileInputStream(file));
        byte[] buf = new byte[1024];
        int len = 0;
        String fileName = "sellmoban.xls";
        response.reset();
        response.setContentType("application/x-msdownload");
        response.setHeader("Content-Disposition", "attachment; filename=" + new String((fileName).getBytes(), "iso8859-1"));
        OutputStream out = response.getOutputStream();
        while ((len = br.read(buf)) > 0)
            out.write(buf, 0, len);
        br.close();
        out.close();

    }

    @RequestMapping( "/uploadSellfile")
    public String file(MultipartFile sellfile, Model model) throws Exception {

        if(sellfile.isEmpty()){
            try {
                throw new Exception("文件不存在！");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        InputStream in =null;
        try {
            in = sellfile.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<List<Object>> listob = null;
        try {
            listob = new ExcelUtil().getBankListByExcel(in,sellfile.getOriginalFilename());

        } catch (Exception e) {
            e.printStackTrace();
        }
        List<Sell> sells = new ArrayList<>();
        //将数据传到数据库中
        for (int i = 0; i < listob.size(); i++) {
            List<Object> lo = listob.get(i);
            if(String.valueOf(lo.get(1))!="") {
                Sell s = new Sell();
                Goods goods = new Goods();
                goods.setGid(Integer.parseInt(lo.get(0).toString()) );
                s.setGood(goods);
                s.setSnum(Integer.parseInt(lo.get(1).toString()));
                s.setSprice(Float.parseFloat(lo.get(2).toString()));
                String msg = String.valueOf(lo.get(3));
                if("".equals(msg)||null==msg){
                    s.setSmsg("无");
                }else{
                    s.setSmsg(msg);
                }
                sells.add(s);
            }
        }
        int count = sellFeign.add(sells);
        model.addAttribute("msg","共添加了"+count+"条数据");
        return "forward:/querySell/1";
    }

}
