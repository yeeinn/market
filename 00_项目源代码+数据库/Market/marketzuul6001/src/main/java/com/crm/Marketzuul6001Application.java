package com.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = "com.crm.feigns")
@EnableZuulProxy
public class Marketzuul6001Application {

    public static void main(String[] args) {
        SpringApplication.run(Marketzuul6001Application.class, args);
    }

}
