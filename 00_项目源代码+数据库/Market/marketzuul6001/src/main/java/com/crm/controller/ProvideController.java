package com.crm.controller;

import com.crm.entity.Provide;
import com.crm.entity.QueryProvide;
import com.crm.feigns.ProvideFeign;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: skj
 * \* Date: 2020/4/7
 * \* Time: 11:28
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
public class ProvideController {
    public ProvideFeign getProvideFeign() {
        return provideFeign;
    }

    public void setProvideFeign(ProvideFeign provideFeign) {
        this.provideFeign = provideFeign;
    }

    @Resource
    private ProvideFeign provideFeign;

    @RequestMapping("queryAllprovide")
    public String queryAllprovide(Model model){
        List<Provide> provide = provideFeign.queryAllprovide();
        model.addAttribute("provide",provide);
        return "provide";
    }
    @RequestMapping("queryByProvide")
    public String queryByProvide(QueryProvide queryProvide,Model model){
        Map<String,Object> map= provideFeign.queryByProvide(queryProvide);
        List<Provide> provideList = (List<Provide>) map.get("provideList");
        model.addAttribute("provideList",provideList);
        model.addAttribute("queryProvide",map.get("queryProvide"));
        return "provide";
    }

    @GetMapping("deleteByPid/{pid}")
    @ResponseBody
    public boolean deleteByPid(@PathVariable Integer pid){
       return provideFeign.deleteByPid(pid);
    }
    @PostMapping("updateByPid")
    @ResponseBody
    public boolean updateByPid(Provide provide){
        return provideFeign.updateByPid(provide);
    }
    @GetMapping("queryByPid/{pid}")
    @ResponseBody
    public Provide queryByPid(@PathVariable Integer pid){
        return provideFeign.queryByPid(pid);
    }

    @GetMapping("addprovide")
        private String addprovide() {
            return "addprovide";
        }

        @PostMapping("addprovide")
        private String addprovide(Provide provide, Model model, HttpServletRequest request) {
            Provide p = provideFeign.selectBypname(provide.getPname());
            String pname = request.getParameter("pname");
            String ptelephone = request.getParameter("ptelephone");
            String paddress = request.getParameter("paddress");
            if (p == null) {
                if (pname != "" && ptelephone != "" && paddress != "" && provideFeign.addProvide(provide)) {
                    model.addAttribute("msg", "添加成功啦！");
                    return "redirect:/queryByProvide";
                } else {
                    model.addAttribute("msg", "信息不能为空哦！");
                    return addprovide();
                }
            } else {
                model.addAttribute("msg", "供应商已经存在不用重复添加啦！");
                return addprovide();
            }

    }
}