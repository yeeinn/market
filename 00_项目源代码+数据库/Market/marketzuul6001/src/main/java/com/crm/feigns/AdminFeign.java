package com.crm.feigns;

import com.crm.entity.Admin;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: jm
 * \* Date: 2020/4/5
 * \* Time: 22:22
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@FeignClient(name = "marketserver8001")
public interface AdminFeign {
    @PostMapping("login")
    public Map<String,Object> login(@RequestParam String aname, @RequestParam String pwd);
    @PostMapping("updatePwd")
    public Map<String,Object> updatePwd(@RequestBody Admin admin);
}
