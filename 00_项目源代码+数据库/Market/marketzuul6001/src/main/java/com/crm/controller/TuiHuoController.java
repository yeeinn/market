package com.crm.controller;

import com.crm.entity.QueryTui;
import com.crm.entity.Sell;
import com.crm.entity.Tuihuo;
import com.crm.feigns.TuiHuoFeign;
import org.hibernate.validator.constraints.EAN;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: dmz
 * \* Date: 2020/4/6
 * \* Time: 下午 05:01
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Controller
public class TuiHuoController {
    @Resource
    private TuiHuoFeign feign;

    @PostMapping("addTH")
    @ResponseBody
    public boolean addTH(Tuihuo tuihuo){
        return feign.addTH(tuihuo);
    }

    @PostMapping("updateTH")
    @ResponseBody
    public boolean updateTH(Tuihuo tuihuo){
        return feign.updateTH(tuihuo);
    }

    @GetMapping("delTH/{id}")
    @ResponseBody
    public boolean delTH(@PathVariable int id){
        return feign.delTH(id);
    }

    @GetMapping("queryTH/{id}")
    @ResponseBody
    public Tuihuo queryTH(@PathVariable int id){
        return feign.queryTH(id);
    }

    @RequestMapping("queryTHs")
    public String queryTHs(QueryTui qt, Model model, HttpSession session){
        Map<String,Object> map=feign.queryTHs(qt);
        List<Tuihuo> tuihuos=(List<Tuihuo>)map.get("ths");
        List<Sell> sells=(List<Sell>)map.get("sells");
        model.addAttribute("sells",sells);
        model.addAttribute("ths",tuihuos);
        model.addAttribute("qt",map.get("qt"));
        return "tuihuo";
    }
}