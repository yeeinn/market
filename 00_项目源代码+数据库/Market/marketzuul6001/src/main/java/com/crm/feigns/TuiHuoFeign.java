package com.crm.feigns;

import com.crm.entity.QueryTui;
import com.crm.entity.Tuihuo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@FeignClient(name = "marketserver9001")
public interface TuiHuoFeign {
    @PostMapping("addTH")
    public boolean addTH(@RequestBody Tuihuo tuihuo);

    @PostMapping("updateTH")
    public boolean updateTH(@RequestBody Tuihuo tuihuo);

    @GetMapping("delTH/{id}")
    public boolean delTH(@PathVariable int id);
    @GetMapping("queryTH/{id}")
    public Tuihuo queryTH(@PathVariable int id);
    @PostMapping("queryTHs")
    public Map<String,Object> queryTHs(@RequestBody QueryTui qt);
}