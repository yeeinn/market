package com.crm.feigns;

import com.crm.entity.Provide;
import com.crm.entity.QueryProvide;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@FeignClient(name = "marketserver9001")
public interface ProvideFeign {
    @RequestMapping("queryAllprovide")
    public List<Provide> queryAllprovide();

    @GetMapping("deleteByPid/{pid}")
    public boolean deleteByPid(@PathVariable Integer pid);

    @PostMapping("updateByPid")
    public boolean updateByPid(@RequestBody Provide provide);

    @GetMapping("queryByPid/{pid}")
    public Provide queryByPid(@PathVariable Integer pid);

    @RequestMapping(value = "queryByProvide")
    public Map<String,Object> queryByProvide(@RequestBody QueryProvide queryProvide);

    @PostMapping("addprovide")
    public boolean addProvide(@RequestBody Provide provide);

    @GetMapping("selectBypname/{pname}")
    public Provide selectBypname(@PathVariable String pname);
}
