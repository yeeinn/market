package com.crm.biz.impl;

import com.crm.biz.TuihuoBiz;
import com.crm.dao.SellMapper;
import com.crm.dao.TuihuoMapper;
import com.crm.entity.Goods;
import com.crm.entity.QueryTui;
import com.crm.entity.Sell;
import com.crm.entity.Tuihuo;
import com.crm.feign.JinFeign;
import com.crm.utils.GetSystemTime;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TuihuoBizImpl implements TuihuoBiz {
    @Resource
    private TuihuoMapper mapper;

    @Resource
    private SellMapper sellMapper;


    @Resource
    private JinFeign jinFeign;

    @Override
    public boolean addTH(Tuihuo tuihuo) {
        if(tuihuo!=null){
            GetSystemTime time=new GetSystemTime();
            tuihuo.setTdate(time.getToday());
           Sell sell=sellMapper.selectByPrimaryKey(tuihuo.getSid());
           if(sell!=null&&sell.getSnum()>=tuihuo.getTnum()){
               Goods goods=jinFeign.queryOneGood(sell.getGood().getGid());
               jinFeign.updateGoods(goods.getGname(),tuihuo.getTnum());
               return mapper.insertSelective(tuihuo)>0;
           }else {
               return false;
           }
        }
        return false;
    }

    @Override
    public boolean updateTH(Tuihuo tuihuo) {
        if(tuihuo!=null){
            GetSystemTime time=new GetSystemTime();
            tuihuo.setTdate(time.getToday());
            Sell sell=sellMapper.querySell(tuihuo.getSid());
            if(sell.getSnum()>=tuihuo.getTnum()){
                return mapper.updateByPrimaryKey(tuihuo)>0;
            }else {
                return false;
            }

        }
        return false;
    }

    @Override
    public boolean delTH(int tid) {
        if(tid!=0){

            Tuihuo tuihuo=queryOne(tid);
            Sell sell=sellMapper.selectByPrimaryKey(tuihuo.getSid());
            Goods goods=jinFeign.queryOneGood(sell.getGood().getGid());
            jinFeign.updateGoods2(goods.getGname(),tuihuo.getTnum());
            return mapper.deleteByPrimaryKey(tid)>0;
        }
        return false;
    }

    @Override
    public Tuihuo queryOne(int tid) {
        return mapper.selectByPrimaryKey(tid);
    }

    @Override
    public List<Tuihuo> queryTHs(QueryTui qt) {
        qt.setRows(qt.getRows());
        int size=qt.getRows();
        int page=qt.getPage();//当前页
        int count=mapper.countTHs(qt);
        count=count%size==0?count/size:(count/size)+1;
        if(count==0){
            qt.setPageCount(0);
            return null;
        }
        page=page<1?1:page;
        page=page>count?count:page;
        qt.setRows(size);
        qt.setPage(page);
        qt.setStart((page-1)*size);
        qt.setPageCount(count);
        qt.setNext(page+1);
        qt.setLast(page-1);
        return mapper.queryTHs(qt);
    }

    @Override
    public int countTHs(QueryTui qt) {
        return mapper.countTHs(qt);
    }

    @Override
    public List<Sell> querySells() {
        return sellMapper.selectAll(new Sell());
    }
}