package com.crm.controller;

import com.crm.biz.ProvideBiz;
import com.crm.entity.Provide;
import com.crm.entity.QueryProvide;
import jdk.nashorn.internal.ir.ReturnNode;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: skj
 * \* Date: 2020/4/7
 * \* Time: 9:25
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
public class ProvideController {
    public ProvideBiz getProvideBiz() {
        return provideBiz;
    }

    public void setProvideBiz(ProvideBiz provideBiz) {
        this.provideBiz = provideBiz;
    }

    @Resource
    private ProvideBiz provideBiz;

    @GetMapping("queryAllprovide")
    public List<Provide> queryAllprovide(Model model){
        List<Provide> provides = provideBiz.queryAllprovide();
        model.addAttribute("provide",provides);
        System.out.println(provides.size());
        return provides;
    }
    @RequestMapping("queryByProvide")
    public Map<String,Object> queryByProvide(@RequestBody QueryProvide queryProvide) {
        Map<String, Object> map = new HashMap<>();
        List<Provide> provideList = provideBiz.queryByProvide(queryProvide);
        map.put("provideList", provideList);
        map.put("queryProvide", queryProvide);
        return map;
    }
    @GetMapping("addprovide")
    private String addprovide(){
        return "addprovide";
    }
    @PostMapping("addprovide")
    private boolean addprovide(@RequestBody Provide provide, Model model, HttpServletRequest request){
        Provide p = provideBiz.selectBypname(provide.getPname());
        String pname = request.getParameter("pname");
        String ptelephone = request.getParameter("ptelephone");
        String paddress = request.getParameter("paddress");
        if(p==null){
            if(pname!=""&&ptelephone!=""&&paddress!=""&&provideBiz.addProvide(provide)){
                model.addAttribute("msg","添加成功啦！");
                return true;
            }else {
                model.addAttribute("msg","信息不能为空哦！");
                return false;
            }
            }else {
                model.addAttribute("msg","供应商已经存在不用重复添加啦！");
                return false;
        }

    }

    @GetMapping("selectBypname/{pname}")
    private Provide selectBypname(@PathVariable String pname){
        return provideBiz.selectBypname(pname);
    }

    @GetMapping("deleteByPid/{pid}")
    public boolean delete(@PathVariable int pid){
        return provideBiz.deleteByPid(pid);
    }

    @PostMapping("updateByPid")
    public boolean updateByPid(@RequestBody Provide provide){
        return provideBiz.updateByPid(provide);
    }

    @GetMapping("queryByPid/{pid}")
    public Provide queryByPid(@PathVariable Integer pid){
        return provideBiz.queryByPid(pid);
    }
}