package com.crm.biz;

import com.crm.entity.QueryTui;
import com.crm.entity.Sell;
import com.crm.entity.Tuihuo;

import java.util.List;

public interface TuihuoBiz {
    public boolean addTH(Tuihuo tuihuo);
    public boolean updateTH(Tuihuo tuihuo);
    public boolean delTH(int tid);
    public Tuihuo queryOne(int tid);
    List<Tuihuo> queryTHs(QueryTui qt);
    int countTHs(QueryTui qt);
    public List<Sell> querySells();
}