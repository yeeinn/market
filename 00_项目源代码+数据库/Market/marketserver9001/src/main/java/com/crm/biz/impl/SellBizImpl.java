package com.crm.biz.impl;

import com.crm.biz.SellBiz;
import com.crm.dao.GoodsMapper;
import com.crm.dao.SellMapper;
import com.crm.entity.Goods;
import com.crm.entity.Sell;
import com.crm.utils.DateFormat;
import com.crm.util.ExcelUtil;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lyy
 * \* Date: 2020/4/6
 * \* Time: 20:23
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Service
public class SellBizImpl implements SellBiz {
    private final int PAGE_SIZE=10;
    @Resource
    private SellMapper sellMapper;

    @Override
    public boolean addSell(Sell sell) {

        sell.setStotal(sell.getSprice()*sell.getSnum());
        if(sell!=null){
            long sysdate=System.currentTimeMillis();
            Date date= new Date(sysdate);
            sell.setSdate(DateFormat.getDate(date));

        }

        return sellMapper.insert(sell)>0;
    }

    @Override
    public boolean deleteSell(int sid) {

        return sellMapper.deleteByPrimaryKey(sid)>0;
    }

    @Override
    public boolean updateSell(Sell sell) {
        return sellMapper.updateBySid(sell)>0;
    }

    @Override
    public List<Sell> querySell(Sell sell, int page) {
        PageHelper.startPage(page,PAGE_SIZE);
        return sellMapper.selectAll(sell);
    }

    @Override
    public int sellCount(Sell sell) {
        int count = sellMapper.getCount(sell);
        int pageCount = count%PAGE_SIZE==0?count/PAGE_SIZE:count/PAGE_SIZE+1;
        return pageCount;
    }
    public Sell querySellById(int sid){
        return sellMapper.selectByPrimaryKey(sid);
    }

    /**
     * 上传题目

     * @return
     * @throws Exception
     */
    public int ajaxUploadExcel( List<Sell> sells) {
        int successcount=0;
        for(Sell sell:sells){
            if(addSell(sell)){
                successcount++;
            }
        }
        return successcount;
    }
}
