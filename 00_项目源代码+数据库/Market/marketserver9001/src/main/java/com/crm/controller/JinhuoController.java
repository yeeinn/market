package com.crm.controller;

import com.crm.biz.GoodsBiz;
import com.crm.biz.JinhuoBiz;
import com.crm.biz.ProvideBiz;
import com.crm.entity.Jinhuo;
import com.crm.entity.Provide;
import com.crm.entity.QueryJinhuo;
import com.crm.feign.JinFeign;
import org.hibernate.Session;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zjx
 * \* Date: 2020/4/6
 * \* Time: 20:47
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
@RequestMapping("Jinhuo")
public class JinhuoController {

    @Resource
    private JinhuoBiz jinhuoBiz;

    @Resource
    private ProvideBiz provideBiz;

    @Resource
    private JinFeign jinFeign;


    //查询进货信息
    @RequestMapping("query")
    public List<Jinhuo> query(Model model){
        List<Jinhuo> jinhuoList = jinhuoBiz.queryAllJinhuo();
//        model.addAttribute("jList",jinhuoList);
        return jinhuoList;
    }

    //分页查询进货信息
    @RequestMapping("queryByPage")
    public Map<String,Object> queryByPage(@RequestBody QueryJinhuo queryJinhuo){
        Map<String,Object> map= new HashMap<>();
        //System.out.println("page"+queryJinhuo.getPage());
        List<Jinhuo> jinhuoList = jinhuoBiz.queryAllJinhuo(queryJinhuo);
        //查询供应商
        List<Provide> provides = provideBiz.selectAll();
        map.put("provides",provides);
        map.put("jinhuoList",jinhuoList);
        map.put("queryJinhuo",queryJinhuo);
        return map;
    }

    //删除进货信息
    @RequestMapping("delete")
    @ResponseBody
    public boolean delete(@RequestParam int jid){
        Jinhuo jinhuo = jinhuoBiz.selectByPrimaryKey(jid);
        boolean flag = jinFeign.updateGoods2(jinhuo.getGname(),jinhuo.getJnum());
        System.out.println(flag);
        return jinhuoBiz.delteByJid(jid);
    }

    //修改进货信息
    @RequestMapping("edit")
    @ResponseBody
    public Jinhuo edit(@RequestParam int jid){
//        Jinhuo jinhuo = jinhuoBiz.selectByPrimaryKey(jid);
//        System.out.println(jinhuo);
        return jinhuoBiz.selectByPrimaryKey(jid);
    }

    @RequestMapping("update")
    @ResponseBody
    public boolean update(@RequestBody Jinhuo jinhuo){

       //boolean flag = jinFeign.updateGoods(jinhuo.getGname(),jinhuo.getJnum());
       // System.out.println(flag);
        return jinhuoBiz.updateJinhuo(jinhuo);
    }


    //添加进货信息
    @RequestMapping("add")
    @ResponseBody
    public boolean add(@RequestBody Jinhuo jinhuo){
        String gname = jinhuo.getGname();
        int count = jinFeign.queryGnameCount(gname);
        if (count!=0){
            jinFeign.updateGoods(gname,jinhuo.getJnum());
            return jinhuoBiz.addJinhuo(jinhuo);
        }else {
            jinFeign.insertGoods(gname,jinhuo.getJnum());
            return jinhuoBiz.addJinhuo(jinhuo);
        }
        //return jinhuoBiz.addJinhuo(jinhuo);
    }

}

