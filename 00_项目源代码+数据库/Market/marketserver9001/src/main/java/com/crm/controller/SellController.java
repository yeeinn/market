package com.crm.controller;

import com.crm.biz.SellBiz;
import com.crm.entity.Goods;
import com.crm.entity.Sell;
import com.crm.feign.JinFeign;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: lyy
 * \* Date: 2020/4/7
 * \* Time: 14:16
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
public class SellController {
    @Resource
    private SellBiz sellBiz;
    @Resource
    private JinFeign sellFeign;

    @PostMapping("queryAll/{page}")
    public List<Sell> queryAll(Model model, @RequestBody Sell sell,@RequestParam @PathVariable int page){
        List<Sell> list = sellBiz.querySell(sell,page);
//        for(Sell s:list){
//            s.setSdate(DateFormat.getDate(s.getSdate()));
//        }
        return list;
    }

    @RequestMapping("addsell")
    public boolean addSell(@RequestBody  Sell sell){
        boolean flag = false;
        Goods goods = sellFeign.queryOneGood(sell.getGood().getGid());
        if("".equals(sell.getSprice())||null==sell.getSprice()){

            sell.setSprice(goods.getGprice());
        }
        if(goods.getGnum()<sell.getSnum()){
            return false;
        }
        goods.setGnum(goods.getGnum()-sell.getSnum());
        if(sellFeign.updateBySelladd(goods)>0){
            flag = sellBiz.addSell(sell);
        }
        return flag;
    }

    @RequestMapping("updatesell")
    public boolean updateSell(@RequestBody Sell sell){
        return sellBiz.updateSell(sell);
    }

    @RequestMapping("deletesell")
    public boolean deleteSell(@RequestParam int sid){
        boolean flag =false;
        Sell sell = sellBiz.querySellById(sid);
        Goods goods = sellFeign.queryOneGood(sell.getGood().getGid());
        goods.setGnum(goods.getGnum()+sell.getSnum());
            if(sellFeign.updateBySelladd(goods)>0){
                flag = sellBiz.deleteSell(sid);
            }
        return flag;
        }


    @RequestMapping("pagecount")
    public int pagecount(@RequestBody Sell sell){
       return sellBiz.sellCount(sell);
    }

    @RequestMapping("querySellByID")
    public Sell querySellByID(@RequestParam int sid){
        return sellBiz.querySellById(sid);
    }



    @RequestMapping("addsells")
    public int add(@RequestBody List<Sell> sells){
        for(Sell sell:sells){
            Goods goods = sellFeign.queryOneGood(sell.getGood().getGid());
            if("".equals(sell.getSprice())||null==sell.getSprice()){

                sell.setSprice(goods.getGprice());
            }
            if(goods.getGnum()<sell.getSnum()){
                sell = null;
            }
            goods.setGnum(goods.getGnum()-sell.getSnum());
            if(sellFeign.updateBySelladd(goods)==0){
                sell=null;
            }
        }
        int count = sellBiz.ajaxUploadExcel(sells);
        return count;
    }




}
