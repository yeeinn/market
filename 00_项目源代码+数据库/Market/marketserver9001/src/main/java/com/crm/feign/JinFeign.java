package com.crm.feign;

import com.crm.entity.Goods;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@FeignClient(name = "marketserver8001")
public interface JinFeign {

    @RequestMapping("deleteByJinhuo")
    public boolean updateGoods2(@RequestParam String gname,@RequestParam int gnum);

    @RequestMapping("updateByJinhuo")
    public boolean updateGoods(@RequestParam String gname,@RequestParam int gnum);

    @RequestMapping("queryByJinhuo")
    public int queryGnameCount(@RequestParam String gname);

    @RequestMapping("insertByJinhuo")
    public int insertGoods(@RequestParam String gname,@RequestParam int gnum);

    @PostMapping("queryOneGood")
    public Goods queryOneGood(@RequestParam int gid);

    @RequestMapping("updateBySell")
    public int updateBySelladd(@RequestBody Goods goods);

}
