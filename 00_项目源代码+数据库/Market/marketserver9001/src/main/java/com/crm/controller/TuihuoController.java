package com.crm.controller;

import com.crm.biz.TuihuoBiz;
import com.crm.entity.QueryTui;
import com.crm.entity.Sell;
import com.crm.entity.Tuihuo;
import com.crm.utils.GetSystemTime;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: dmz
 * \* Date: 2020/4/5
 * \* Time: 下午 07:46
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@RestController
public class TuihuoController {

    @Resource
    private TuihuoBiz tuihuoBiz;

    @PostMapping("addTH")
    public boolean addTH(@RequestBody Tuihuo tuihuo){
        if(tuihuo!=null){
            return tuihuoBiz.addTH(tuihuo);
        }
        return false;
    }

    @PostMapping("updateTH")
    public boolean updateTH(@RequestBody Tuihuo tuihuo){
        if(tuihuo!=null){
            return tuihuoBiz.updateTH(tuihuo);
        }
        return false;
    }

    @GetMapping("delTH/{id}")
    public boolean delTH(@PathVariable int id){
        if(id!=0){
            return tuihuoBiz.delTH(id);
        }
        return false;
    }
    @GetMapping("queryTH/{id}")
    public Tuihuo queryTH(@PathVariable int id){
        return tuihuoBiz.queryOne(id);
    }


    @PostMapping("queryTHs")
    public Map<String,Object> queryTHs(@RequestBody QueryTui qt){
        Map<String,Object> map= new HashMap<>();
        List<Tuihuo> tuihuos=tuihuoBiz.queryTHs(qt);
        List<Sell> sells=tuihuoBiz.querySells();
        map.put("ths",tuihuos);
        map.put("sells",sells);
        map.put("qt",qt);
        return map;
    }
}