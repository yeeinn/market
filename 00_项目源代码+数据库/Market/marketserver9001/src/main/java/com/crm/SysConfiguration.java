package com.crm;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.github.pagehelper.PageInterceptor;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;


/**
 * \* Created with IntelliJ IDEA.
 * \* User: lyy
 * \* Date: 2020/3/23
 * \* Time: 14:05
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Configuration
public class SysConfiguration {

    @Bean
    public PageInterceptor pageHelper(){
        PageInterceptor pegeInterceptor = new PageInterceptor();
        Properties properties = new Properties();
        properties.setProperty("helperDialect", "mysql");  //数据库
        properties.setProperty("reasonable", "true");  //合理化分页上下容错
        properties.setProperty("supportMethodsArguments", "true");
        properties.setProperty("autoRuntimeDialect", "true");
        pegeInterceptor.setProperties(properties);
        return pegeInterceptor;
    }




}
