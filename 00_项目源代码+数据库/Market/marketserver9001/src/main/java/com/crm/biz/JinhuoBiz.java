package com.crm.biz;


import com.crm.entity.Jinhuo;
import com.crm.entity.QueryJinhuo;

import java.util.List;

public interface JinhuoBiz {

    public List<Jinhuo> queryAllJinhuo();
    public boolean delteByJid(int jid);
    public boolean updateJinhuo(Jinhuo jinhuo);

    public boolean addJinhuo(Jinhuo jinhuo);

    public List<Jinhuo> queryAllJinhuo(QueryJinhuo queryJinhuo);

    public int queryJinhuoCount(QueryJinhuo queryJinhuo);

    Jinhuo selectByPrimaryKey(Integer jid);
}
