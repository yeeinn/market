package com.crm.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zjx
 * \* Date: 2020/4/7
 * \* Time: 10:24
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class DateUtil {

    public static final String FORMAT = "yyyy-MM-dd";

    public static String getNowDay(){
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
        Date date = new Date();
        String time = sdf.format(date);
        return time;
    }

}

