package com.crm.biz;

import com.crm.entity.Provide;
import com.crm.entity.QueryProvide;

import java.util.List;

public interface ProvideBiz {
    public List<Provide> queryAllprovide();
    public boolean deleteByPid(Integer pid);
    public boolean updateByPid(Provide provide);
    public Provide queryByPid(Integer pid);
    public List<Provide> queryByProvide(QueryProvide queryProvide);
    public int queryByProvideCount(QueryProvide queryProvide);
    public boolean addProvide(Provide provide);
    public Provide selectBypname(String pname);


    public List<Provide> selectAll();
}
