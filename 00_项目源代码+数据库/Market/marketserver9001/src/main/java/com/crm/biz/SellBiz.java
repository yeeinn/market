package com.crm.biz;

import com.crm.entity.Sell;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
@Service
public interface SellBiz {


    public boolean addSell(Sell sell);
    public boolean deleteSell(int sid);
    public boolean updateSell(Sell sell);
    public List<Sell> querySell(Sell sell,int page);
    public Sell querySellById(int sid);

    public int sellCount(Sell sell);

    public int ajaxUploadExcel(List<Sell> sells) ;
}
