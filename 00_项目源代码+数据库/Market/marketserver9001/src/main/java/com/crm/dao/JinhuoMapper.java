package com.crm.dao;

import com.crm.entity.Jinhuo;

import java.util.List;

import com.crm.entity.JinhuoExample;
import com.crm.entity.QueryJinhuo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface JinhuoMapper {
    long countByExample(JinhuoExample example);

    int deleteByExample(JinhuoExample example);

    int deleteByPrimaryKey(Integer jid);

    int insert(Jinhuo record);

    int insertSelective(Jinhuo record);

    List<Jinhuo> selectByExample(JinhuoExample example);

    Jinhuo selectByPrimaryKey(Integer jid);

    int updateByExampleSelective(@Param("record") Jinhuo record, @Param("example") JinhuoExample example);

    int updateByExample(@Param("record") Jinhuo record, @Param("example") JinhuoExample example);

    int updateByPrimaryKeySelective(Jinhuo record);

    int updateByPrimaryKey(Jinhuo record);

    List<Jinhuo> queryAllJinhuo();

    List<Jinhuo> queryAllJinhuo(QueryJinhuo queryJinhuo);

    int queryJinhuoCount(QueryJinhuo queryJinhuo);

    Jinhuo queryByJid(int jid);
}