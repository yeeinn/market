package com.crm.dao;

import com.crm.entity.QueryTui;
import com.crm.entity.Tuihuo;
import com.crm.entity.TuihuoExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface TuihuoMapper {
    long countByExample(TuihuoExample example);

    int deleteByExample(TuihuoExample example);

    int deleteByPrimaryKey(Integer tid);

    int insert(Tuihuo record);

    int insertSelective(Tuihuo record);

    List<Tuihuo> selectByExample(TuihuoExample example);

    Tuihuo selectByPrimaryKey(Integer tid);

    int updateByExampleSelective(@Param("record") Tuihuo record, @Param("example") TuihuoExample example);

    int updateByExample(@Param("record") Tuihuo record, @Param("example") TuihuoExample example);

    int updateByPrimaryKeySelective(Tuihuo record);

    int updateByPrimaryKey(Tuihuo record);

    List<Tuihuo> queryTHs(QueryTui qt);

    int countTHs(QueryTui qt);

}