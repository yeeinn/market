package com.crm.biz.impl;

import com.crm.biz.ProvideBiz;
import com.crm.dao.ProvideMapper;
import com.crm.entity.Provide;
import com.crm.entity.ProvideExample;
import com.crm.entity.QueryProvide;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: skj
 * \* Date: 2020/4/7
 * \* Time: 8:59
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
@Service
public class ProvideBizImpl implements ProvideBiz {

    public ProvideMapper getProvideMapper() {
        return provideMapper;
    }

    public void setProvideMapper(ProvideMapper provideMapper) {
        this.provideMapper = provideMapper;
    }

    @Resource
    private ProvideMapper provideMapper;
    @Override
    public List<Provide> queryAllprovide() {
        return provideMapper.selectByExample(null);
    }

    @Override
    public boolean deleteByPid(Integer pid) {
        return provideMapper.deleteByPrimaryKey(pid)>0;
    }

    @Override
    public boolean updateByPid(Provide provide) {
        return provideMapper.updateByPrimaryKey(provide)>0;
    }

    @Override
    public Provide queryByPid(Integer pid) {
        return provideMapper.selectByPrimaryKey(pid);
    }

    @Override
    public List<Provide> queryByProvide(QueryProvide queryProvide) {
        int size=queryProvide.getRows();
        int page=queryProvide.getPage();
        int count=provideMapper.queryByProvideCount(queryProvide);
        count=count%size==0?count/size:count/size+1;
        if(count==0){
            return null;
        }
        page=page<1?1:page;
        page=page>count?count:page;
        queryProvide.setPage(page);
        queryProvide.setStart((page-1)*size);
        queryProvide.setPagecount(count);
        queryProvide.setNext(page-1);
        queryProvide.setLast(page+1);//下一页
        return provideMapper.queryByProvide(queryProvide);
    }

    @Override
    public int queryByProvideCount(QueryProvide queryProvide) {
        return provideMapper.queryByProvideCount(queryProvide);
    }

    @Override
    public boolean addProvide(Provide provide) {
        return provideMapper.addProvide(provide);
    }

    @Override
    public Provide selectBypname(String pname) {
        return provideMapper.selectBypname(pname);
    }

    @Override
    public List<Provide> selectAll() {
        return provideMapper.selectAll();
    }

}
