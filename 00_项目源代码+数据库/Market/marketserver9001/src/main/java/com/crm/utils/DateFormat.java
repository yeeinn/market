package com.crm.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zjx
 * \* Date: 2020/4/7
 * \* Time: 10:24
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */
public class DateFormat {

    public static final String FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String getDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
        String time = sdf.format(date);
        return time;
    }

}

