package com.crm.dao;

import com.crm.entity.Provide;
import com.crm.entity.ProvideExample;
import java.util.List;

import com.crm.entity.QueryProvide;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface ProvideMapper {
    long countByExample(ProvideExample example);

    int deleteByExample(ProvideExample example);

    int deleteByPrimaryKey(Integer pid);

    int insert(Provide record);

    int insertSelective(Provide record);

    List<Provide> selectByExample(ProvideExample example);

    Provide selectByPrimaryKey(Integer pid);

    int updateByExampleSelective(@Param("record") Provide record, @Param("example") ProvideExample example);

    int updateByExample(@Param("record") Provide record, @Param("example") ProvideExample example);

    int updateByPrimaryKeySelective(Provide record);

    int updateByPrimaryKey(Provide record);

    List<Provide> selectAll();

    public List<Provide> queryByProvide(QueryProvide queryProvide);

    public int queryByProvideCount(QueryProvide queryProvide);

    public boolean addProvide(Provide provide);

    public Provide selectBypname(String pname);
    
}