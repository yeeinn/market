package com.crm.biz.impl;

import com.crm.biz.JinhuoBiz;
import com.crm.dao.JinhuoMapper;
import com.crm.entity.Jinhuo;
import com.crm.entity.QueryJinhuo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: zjx
 * \* Date: 2020/4/6
 * \* Time: 20:51
 * \* To change this template use File | Settings | File Templates.
 * \* Description:
 * \
 */

@Service
public class JinhuoBizImpl implements JinhuoBiz {

    public static final String FORMAT = "yyyy-MM-dd";

    @Resource
    private JinhuoMapper jinhuoMapper;

    @Override
    public List<Jinhuo> queryAllJinhuo() {
        return jinhuoMapper.queryAllJinhuo();
    }

    @Override
    public boolean delteByJid(int jid) {
        return jinhuoMapper.deleteByPrimaryKey(jid)>0;
    }

    @Override
    public boolean updateJinhuo(Jinhuo jinhuo) {
        return jinhuoMapper.updateByPrimaryKeySelective(jinhuo)>0;
    }

    @Override
    public boolean addJinhuo(Jinhuo jinhuo) {
//        String jdate = DateUtil.getNowDay();
//        Date date = new Date(jdate);
//        jinhuo.setJdate(date);
        return jinhuoMapper.insertSelective(jinhuo)>0;
    }

    @Override
    public List<Jinhuo> queryAllJinhuo(QueryJinhuo queryJinhuo) {
        int size = queryJinhuo.getRows();  //每页显示几条
        int page = queryJinhuo.getPage();  //当前页
        int count = jinhuoMapper.queryJinhuoCount(queryJinhuo);  //总条数
        //System.out.println(count);
        count=count%size==0?count/size:(count/size)+1;  //总页数
        if (count==0){
            queryJinhuo.setPageCount(0);
            return null;
        }
        page = page<1?1:page;
        page = page>count?count:page;
        queryJinhuo.setPage(page);
        queryJinhuo.setStart((page-1)*size);   //偏移量
        queryJinhuo.setPageCount(count);   //总页数
        queryJinhuo.setNext(page+1);
        queryJinhuo.setLast(page-1);
       // System.out.println(queryJinhuo);
        return jinhuoMapper.queryAllJinhuo(queryJinhuo);
    }

    @Override
    public int queryJinhuoCount(QueryJinhuo queryJinhuo) {
        return jinhuoMapper.queryJinhuoCount(queryJinhuo);
    }

    @Override
    public Jinhuo selectByPrimaryKey(Integer jid) {
        return jinhuoMapper.selectByPrimaryKey(jid);
    }

}

